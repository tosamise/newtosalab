## To create new project
$ git clone https://tosamise@bitbucket.org/tosamise/newtosalab.git

## Add database
- On local edit .env file
- On production server, edit /config/database.php file

## Using composer to get vendor folder
$ composer update

## Laravel using version is 5.4.x  