<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Fontawesome CSS -->
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">

    <!-- Justified-Gallery -->
    <link href="{{ asset('css/justifiedGallery.min.css') }}" rel="stylesheet">

    <!-- Data table -->
    <link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet">

    <!-- Magnific Popup -->
    <link href="{{ asset('css/magnific-popup.css') }}" rel="stylesheet">

    <!-- Toastr CSS -->
    <link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet">

    <!-- Common CSS -->
    <link href="{{ asset('css/common.css') }}" rel="stylesheet">

    <!-- Light box -->
    <link href="{{ asset('css/blueimp-gallery.min.css') }}" rel="stylesheet">

    <!-- Selectize.js -->
    <link href="{{ asset('css/selectize.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/backend.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
      window.Laravel = {!! json_encode([
          'csrfToken' => csrf_token(),
      ]) !!};
    </script>
  </head>

  <body>

    <nav class="navbar navbar-fixed-top bg-color-white">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand custom-font" href="/">{{ config('app.name', 'Laravel') }}</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <!-- Right Side Of Navbar -->
          <ul class="nav navbar-nav navbar-right">
              <!-- Authentication Links -->
              @if (Auth::guest())
                  <!-- <li><a href="{{ route('register') }}">Register</a></li> -->
              @else
                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <i class="fa fa-user-circle" aria-hidden="true"></i>
                        {{ Auth::user()->name }} <span class="caret"></span>
                      </a>

                      <ul class="dropdown-menu" role="menu">
                          <li>
                              <a href="{{ route('logout') }}"
                                  onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                                  Logout
                              </a>

                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  {{ csrf_field() }}
                              </form>
                          </li>
                      </ul>
                  </li>
              @endif
          </ul>
        </div>
      </div>
    </nav>

    {{ $action = Request::segment(1) }}
    <div class="container-fluid">
      <div class="row">
          <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
              <li class="{{ $action === 'home' ? 'active' : '' }}">
                <a href="/admin/home">
                  Overview <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="{{ $action === 'gallery' ? 'active' : '' }}">
                <a href="{{ route('gallery') }}">Gallery</a>
              </li>
              <li id="camera_menu" class="">
                <a id="camera_info" href="javascript:void(0)" data-toggle="collapse" data-target="#submenu_camera" aria-expanded="false">Camera info <i class="fa fa-angle-down fr"></i></a>
                <ul id="submenu_camera" class="nav collapse ml20" role="menu" aria-labelledby="camera_info">
                  {{--<li>--}}
                    {{--<a href="/admin/info/add-camera">Add new</a>--}}
                  {{--</li>--}}
                  <li>
                    <a href="/admin/info/camera">Camera list</a>
                  </li>
                </ul>
              </li>
              <li id="film_menu" class="">
                <a id="film_info" href="javascript:void(0)" data-toggle="collapse" data-target="#submenu_film" aria-expanded="false">Film info <i class="fa fa-angle-down fr"></i></a>
                <ul id="submenu_film" class="nav collapse ml20" role="menu" aria-labelledby="film_info">
                  {{--<li>--}}
                    {{--<a href="/admin/info/film">Add new</a>--}}
                  {{--</li>--}}
                  <li>
                    <a href="/admin/info/film">Film list</a>
                  </li>
                </ul>
              </li>
              <li id="roll_menu" class="">
                <a id="roll_info" href="javascript:void(0)" data-toggle="collapse" data-target="#submenu_roll" aria-expanded="false">Roll info <i class="fa fa-angle-down fr"></i></a>
                <ul id="submenu_roll" class="nav collapse ml20" role="menu" aria-labelledby="film_roll">
                  {{--<li>--}}
                    {{--<a href="/admin/info/roll" title="Add new roll">Add new</a>--}}
                  {{--</li>--}}
                  <li>
                    <a href="/admin/info/roll" title="Roll list">Roll list</a>
                  </li>
                </ul>
              </li>
              <li id="setting_menu" class="">
                <a id="film_info" href="javascript:void(0)" data-toggle="collapse" data-target="#submenu_setting" aria-expanded="false">Settings <i class="fa fa-angle-down fr"></i></a>
                @if( in_array( Auth::user()->role , [1] ) )
                <ul id="submenu_setting" class="nav collapse ml20" role="menu" aria-labelledby="setting_menu">
                  <li>
                    <a href="/admin/register">Register user</a>
                  </li>
                  <li>
                    <a href="/admin/list-user">List user</a>
                  </li>
                </ul>
                @endif
              </li>
            </ul>
          </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
              @if(Session::has('alert-' . $msg))
              <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
              @endif
            @endforeach
          </div> <!-- end .flash-message -->

          @yield('content')
        </div>
      </div>
    </div>

    <!-- Loading image when ajax process -->
    <div class="loading-area" style="background: rgba( 255, 255, 255, .8 ) url('/images/loading.gif') 50% 50% no-repeat;"></div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Scripts -->
    <script src="{{ asset('js/jquery-3.2.0.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- Justified-Gallery -->
    <script src="{{ asset('js/jquery.justifiedGallery.min.js') }}"></script>
    <!-- Data table -->
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <!-- Magnific popup -->
    <script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
    <!-- Lightbox -->
    <script src="{{ asset('js/blueimp-gallery.min.js') }}"></script>
    <!-- Toastr -->
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <!-- Selectize -->
    <script src="{{ asset('js/selectize.js') }}"></script>
    <!-- Jquery validation -->
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <!-- backend custom JS -->
    <script src="{{ asset('js/backend.js') }}"></script>
    
    <script type="text/javascript">
      //Load loading image when run ajax
      $body = $("body");
      $(document).on({
        ajaxStart: function() { $body.addClass("loading"); },
        ajaxStop: function() { $body.removeClass("loading"); }    
      });
    </script>
    
    <!-- View specific js -->
    @yield('page-script')
  </body>
</html>