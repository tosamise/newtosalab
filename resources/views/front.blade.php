<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Fontawesome CSS -->
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">

    <!-- Justified-Gallery -->
    <link href="{{ asset('css/justifiedGallery.min.css') }}" rel="stylesheet">
    
    <!-- Common CSS -->
    <link href="{{ asset('css/common.css') }}" rel="stylesheet">

    <!-- Light box -->
    <link href="{{ asset('css/blueimp-gallery.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/backend.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
      window.Laravel = {!! json_encode([
          'csrfToken' => csrf_token(),
      ]) !!};
    </script>
  </head>

  <body>

    <nav class="navbar navbar-fixed-top bg-color-white">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand custom-font" href="/">{{ config('app.name', 'Laravel') }}</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <!-- Right Side Of Navbar -->
          <ul class="nav navbar-nav navbar-right">
              <!-- Authentication Links -->
              @if (Auth::guest())
                <li><a href="{{ route('login') }}">Login</a></li>
              @else
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <i class="fa fa-user-circle" aria-hidden="true"></i>
                    {{ Auth::user()->name }} <span class="caret"></span>
                  </a>

                  <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ url('/admin/home') }}">Manage Page</a></li>
                    <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                  </ul>
                </li>
              @endif
          </ul>
        </div>
      </div>
    </nav>

    <div id="welcome_gallery">
    @foreach($images as $image)
    @php
      $url = "/images/gallery/";
      $apature = ($image->apature)?"f".$image->apature:"Unknown";
      $shutter = ($image->shutter)?$image->shutter:"Unknown";
      $title = ($image->title)?$image->title:"Unknown";
    @endphp
      <a href="{{ URL::to('/').$url.$image->file_name }}" title="{{ $title }} | {{ $apature }} | {{ $shutter }}">
          <img alt="{{ $apature }} | {{ $shutter }}" src="{{ URL::to('/').$url.$image->file_name }}" class=""/>
      </a>
    @endforeach
    </div>

    <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
    <div id="blueimp-gallery" class="blueimp-gallery">
      <div class="slides"></div>
      <h3 class="title"></h3>
      <a class="prev">‹</a>
      <a class="next">›</a>
      <a class="close">×</a>
      <a class="play-pause"></a>
      <ol class="indicator"></ol>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Scripts -->
    <script src="{{ asset('js/jquery-3.2.0.min.js') }}"></script>
    <!-- <script src="{{ asset('js/jquery.min.js') }}"></script> -->
    <!-- Bootstrap -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- Justified-Gallery -->
    <script src="{{ asset('js/jquery.justifiedGallery.min.js') }}"></script>
    <!-- Lightbox -->
    <script src="{{ asset('js/blueimp-gallery.min.js') }}"></script>
    <!-- View specific js -->
    <script type="text/javascript">

      $("#welcome_gallery").justifiedGallery({
        rowHeight:200,
        margins: 10
      });
      $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() == $(document).height()) {
          for (var i = 0; i < 5; i++) {
            $('#welcome_gallery').append('<a>' +
                '<img src="{{ URL::to('/') }}/images/000006.JPG" />' + 
                '</a>');
          }
          $('#welcome_gallery').justifiedGallery('norewind');
        }
      });

      document.getElementById('welcome_gallery').onclick = function (event) {
        event = event || window.event;
        var target = event.target || event.srcElement,
            link = target.src ? target.parentNode : target,
            options = {index: link, event: event},
            links = this.getElementsByTagName('a');
        blueimp.Gallery(links, options);
      };




    </script>
  </body>
</html>