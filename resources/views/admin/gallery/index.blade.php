@extends('layouts.backend')

@section('content')
<link href="{{ asset('css/leaflet.css') }}" rel="stylesheet">
<h1 class="page-header hidden-xs ">Gallery</h1>
	<div id="admin_gallery" >
	@foreach($images as $image)
	@php
		$url = "/images/gallery/";
        $src = URL::to('/').$url.$image->file_name;
		$apature = ($image->apature)?$image->apature:"Unknown";
		$shutter = ($image->shutter)?$image->shutter:"Unknown";
		$title = ($image->title)?$image->title:"Unknown";
	@endphp
    <a href="#detail_popup" 
       title="{{ $title }} | {{ $apature }} | {{ $shutter }}" 
       class="show_detail"
       data-id="{{ $image->id }}"
      >
      <img alt="{{ $apature }} | {{ $shutter }}" src="{{ $src }}" class=""/>
    </a>
	@endforeach
	</div>

	<!-- Image detail -->
  <div id="detail_popup" class="custom_popup w90p mfp-hide fade2" style="background-color: #212126">
    <div class="box-body">
        <div class="row content-wrap">
          <div class="col-md-9 text-c left-content-wrap">
            <div class="text-c image-wrap">
              <img src="{{$url}}000001.JPG" id="preview_image" class="col-md-12 text-c fn">
            </div>
          </div>
          <div class="col-md-3 right-content-wrap">
            <div class="desc-wrap">
              <div class="avatar col-md-5">
                <img id="avatar" src="/images/avatar/no-avatar.png" />
              </div>
              <div class="col-md-7">
                <span class="uc" id="popup_name">Unknown</span><br>
                <i id="popup_role">Guest</i><br>
                <span id="popup_created_at">Joined: <span>Unknown</span></span>
              </div>
              <div class="col-md-12 mt20">
                <div class="exif">
                  <i id="popup_title">Unknown</i><br>
                  <a id="popup_camera" href="#">#<span>Unknown</span></a><br>
                  <a id="popup_film" href="#">#<span>Unknown</span></a>
                  <span> - </span>
                  <a id="popup_roll" href="#">#<span>Unknown</span></a>
                  <br>
                  <span id="popup_info"></span><br>
                  <a id="popup_lab" href="#">#<span>Unknown</span></a><br>
                </div>
                <div class="col-md-12 mt20 map p0" id=""></div>
              </div>
            </div>
          </div>
        </div>
        <button class='close_btn btn btn-default fr pb40 noradius'><i class="fa fa-times"></i> Close</button>
        <a href="#" id="edit_btn" class='btn btn-default fr pb40 mr10 noradius'><i class="fa fa-pencil"></i> Edit</a>
    </div>
    <!-- /.box-body -->
  </div>

@section('page-script')
<script src="{{ asset('js/leaflet.js') }}"></script>
<script type="text/javascript">

  //Init the justified Gallery
	$("#admin_gallery").justifiedGallery({
    rowHeight:200,
    margins: 10
  });

  //Lazy loading
  // $(window).scroll(function() {
  //   if($(window).scrollTop() + $(window).height() == $(document).height()) {
  //     for (var i = 0; i < 5; i++) {
  //       $('#admin_gallery').append('<a>' +
  //           '<img src="{{ URL::to('/').$url }}000001.JPG" \/>' + 
  //           '</a>');
  //     }
  //     $('#admin_gallery').justifiedGallery('norewind');
  //   }
  // });

  //Initialize the magnific popup IMAGE DETAIL
  $(function () {
    $('.show_detail').magnificPopup({
      //type: 'inline',
      preloader: false,
      focus: '#detail_popup',
      closeBtnInside:true,
      modal: true,
      callbacks:{
        beforeOpen: function() {
          $("#preview_image").attr( "src" , "");
          $("#map").html("");
        },
        open: function(){
          //Get button element when click popup00
          var mp = $.magnificPopup.instance;
          curItem = $(mp.currItem.el[0]);
          var url = "{{ $url }}";
          $.ajax({
            type: "POST",
            url: "/admin/ajax-gallery",
            dataType: 'json',
            data: {"_token": "{{ csrf_token() }}",
                   "action": "get",
                   "id": curItem.data('id')
                  },
            success: function(data){
              if( data[0] != "" )
              {
                data = data[0];
                //Image
                if( data['file_name'] != "" ){
                  $("#preview_image").attr( "src", url+data['file_name'] );
                }
                //User avatar
                if( data['avatar'] != "" ){
                  $("#avatar").attr( "src", url+"../avatar/"+data['avatar'] );
                }
                //User name
                if( data['name'] )
                {
                  $("#popup_name").html( data['name'] );
                }
                //Date joined - created_at
                if( data['created_at'] )
                {
                  var date = new Date();
                  var month = date.toLocaleString("en-us", { month: "long" });
                  var format_date = month+" "+date.getDay()+", "+date.getFullYear()
                  $("#popup_created_at").children("span").html( format_date );
                }
                //Role
                if( data['role'] )
                {
                  $("#popup_role").html( data['role'] );
                }
                //Title
                if( data['title'] )
                {
                  $("#popup_title").html( data['title'] );
                }
                //Apature & Shutter
                if( data['apature'] && data['shutter'] )
                {
                  $("#popup_info").html( data['apature']+" | "+data['shutter'] );
                }
                //Apature & Shutter
                if( data['apature'] && data['shutter'] )
                {
                  $("#popup_info").html( data['apature']+" | "+data['shutter'] );
                }
                //Roll info
                if( data['code_film'] )
                {
                  $("#popup_roll").html( data['code_film'] );
                }
                //Camera
                if( data['cam_name'] && data['lens'] )
                {
                  $("#popup_camera").children("span").html( data['cam_name']+" - "+data['lens'] );
                }
                //Lab
                if( data['lab_name'] )
                {
                  $("#popup_lab").children("span").html( data['lab_name'] );
                }
                //Film
                if( data['film_name'] )
                {
                  $("#popup_film").children("span").html( data['film_name'] );
                }

                //Generate map
                var coordinates = data['coordinates'].split(",");
                var lat = coordinates[0];
                var lng = coordinates[1];
                createPopupMap(lat,lng);

                //Edit button
                $("#edit_btn").attr("href","/admin/gallery/edit/"+data['id']+"");

              }
            },
          });
        }
      }
    });
    $(document).on('click', '.close_btn', function (e) {
      e.preventDefault();
      $.magnificPopup.close();
    });
  });

  function createPopupMap(lat,lng)
  {
    $('.map').html('<div class="col-md-12" id="mapidCustom"></div>');
    var mymap = L.map('mapidCustom').setView([lat, lng], 10);
    var mapLink = '<a href="https://openstreetmap.org" target="_blank">OpenStreetMap</a>';
    
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; ' + mapLink,
        maxZoom: 18,
    }).addTo(mymap);
    
    var myIcon = L.icon({
        iconUrl: '/img/lmarker01.png',
        iconSize: [134, 49],
        iconAnchor: [0, 0],
        popupAnchor: [0, 0]
    });
    L.marker([lat, lng]).addTo(mymap); 
  }

</script>
@stop
@endsection