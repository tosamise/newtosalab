@extends('layouts.backend')

@section('content')
<link href="{{ asset('css/leaflet.css') }}" rel="stylesheet">
<h1 class="page-header hidden-xs ">Edit image</h1>
	<div class="col-md-12" id="admin_gallery_edit" >
    <div class="panel panel-default">
      <div class="panel-body">

        <div class="col-md-4">
          {{ Form::model( $images, ['url' => ['/admin/gallery/update', $images->id], 'method' => 'post', 'role' => 'form'] ) }}
          {{ csrf_field() }}

            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
              <div class="">
                {{ Form::text('title', null, ['class'=>'form-control']) }}

                @if ($errors->has('title'))
                  <span class="help-block">
                    <strong>{{ $errors->first('title') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="form-group{{ $errors->has('roll_id') ? ' has-error' : '' }}">
              <div class="">
                {{ Form::select('roll_id', $rolls ,null, ['class'=>'form-control']) }}

                @if ($errors->has('roll_id'))
                  <span class="help-block">
                    <strong>{{ $errors->first('roll_id') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            
            @php
              $apature = array_combine( Config::get('info.apature') , Config::get('info.apature') );
            @endphp
            <div class="form-group{{ $errors->has('apature') ? ' has-error' : '' }}">
              <div class="">
                {{ Form::select('apature', $apature ,null, ['class'=>'form-control']) }}

                @if ($errors->has('apature'))
                  <span class="help-block">
                    <strong>{{ $errors->first('apature') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            @php
              $shutter = array_combine( Config::get('info.shutter') , Config::get('info.shutter') );
            @endphp
            <div class="form-group{{ $errors->has('shutter') ? ' has-error' : '' }}">
              <div class="">
                {{ Form::select('shutter', $shutter ,null, ['class'=>'form-control']) }}

                @if ($errors->has('shutter'))
                  <span class="help-block">
                    <strong>{{ $errors->first('shutter') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="form-group{{ $errors->has('shutter') ? ' has-error' : '' }}">
              <div class="">
                {{ Form::select('taken_by', $users ,null, ['class'=>'form-control']) }}

                @if ($errors->has('taken_by'))
                  <span class="help-block">
                    <strong>{{ $errors->first('taken_by') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
              <div class="">
                {{ Form::textarea('description', null, ['class'=>'form-control']) }}

                @if ($errors->has('description'))
                  <span class="help-block">
                    <strong>{{ $errors->first('description') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="form-group{{ $errors->has('coordinates') ? ' has-error' : '' }}">
              <div class="">
                {{ Form::text('coordinates', null, ['class'=>'form-control','id'=>'coordinates']) }}

                @if ($errors->has('coordinates'))
                  <span class="help-block">
                    <strong>{{ $errors->first('coordinates') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <a href="/admin/gallery" class='btn btn-default fr pb40 noradius'><i class="fa fa-image"></i> Gallery</a>
            <button id="update_btn" type="submit" class='btn btn-default fr pb40 mr10 noradius'><i class="fa fa-save"></i> Update</button>
            <a href="#map_popup" id="open_map_btn" class='btn btn-default fr pb40 mr10 noradius'><i class="fa fa-map"></i> Map</a>

          {{ Form::close() }}
        </div>
        <div class="col-md-8">
          @php
            $url = "/images/gallery/";
            $src = URL::to('/').$url.$images->file_name;
          @endphp
          <img src="{{$src}}" id="preview_image" class="col-md-12 text-c fn">
        </div>

      </div>
    </div>
  <!-- <pre>{{ print_r($images) }}</pre> -->
	</div>

  <!-- Image detail -->
  <div id="map_popup" class="custom_popup w90p mfp-hide fade2" style="background-color: #212126">
    <div class="box-body">
        <div class="row content-wrap">
          <div class="col-md-12 map p0 mb20" id="" style="height: 500px"></div>
        </div>      
        <button class='close_btn btn btn-default fr pb40 noradius'><i class="fa fa-times"></i> Close</button>
    </div>
    <!-- /.box-body -->
  </div>

@section('page-script')
<script src="{{ asset('js/leaflet.js') }}"></script>
<script type="text/javascript">

$('#open_map_btn').magnificPopup({
  //type: 'inline',
  preloader: false,
  focus: '#map_popup',
  closeBtnInside:true,
  modal: true,
  callbacks:{
    beforeOpen: function() {

    },
    open: function(){
      var old_coor = $("#coordinates").val();
      var new_lat = "";
      var new_long = "";
      if( old_coor != "")
      {
        var temp = old_coor.split(",");
        new_lat = temp[0];
        new_long = temp[1];
      }
      else
      {
        new_lat = 10.6512;
        new_long = 106.6910;
      }
      //Create map
      $('.map').html('<div class="" id="update_map" style="height:100%"></div>');
      var mymap = L.map('update_map').setView([new_lat,new_long], 10);
      
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
      }).addTo(mymap);
      
      var marker = L.marker([new_lat,new_long]).addTo(mymap);

      //Get action click to map
      function onMapClick(e) 
      {
        var coor = e.latlng.toString();
        var coor_val = coor.substring( coor.indexOf("(")+1 , coor.indexOf(")") );
        lat = coor_val.substring(0, coor_val.indexOf(","));
        long = coor_val.substring(coor_val.indexOf(",")+1);
        // console.log(coor_val);
        $("#coordinates").val( coor_val );
        //Check if existed markers, remove it.
        if (marker) {
          mymap.removeLayer(marker); // remove
        }
        //Add new marker to map on each click
        marker = L.marker([lat, long]).addTo(mymap);
      }
      mymap.on('click', onMapClick);
    }
  }
});

$(document).on('click', '.close_btn', function (e) {
  e.preventDefault();
  $.magnificPopup.close();
});

</script>
@stop
@endsection