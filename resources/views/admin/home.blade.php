@extends('layouts.backend')

@section('content')
<h1 class="page-header hidden-xs ">Dashboard</h1>
<div class="container">
    <div class="row">
        <div class="col-md-11">
            <div class="panel panel-default">
                <div class="panel-body">
                    You are logged in
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
