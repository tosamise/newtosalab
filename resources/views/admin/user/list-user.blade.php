
@extends('layouts.backend')

@section('content')
<?php $url = "/images/avatar/" ?>
<link href="{{ asset('css/dropzone.css') }}" rel="stylesheet">

<h3 class="page-header hidden-xs ">USERS LIST</h3>
<table id="users-list" class="table table-default">
	<thead>
		<tr>
			<th>No.</th>
			<th>Name</th>
			<th>Email</th>
			<th>Role</th>
			<th>Status</th>
			<th>Action</th>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
	<?php $count = 1; ?>
	@foreach( $users as $user )
		<tr>
			<td>{{ $count }}</td>
			<td>{{ $user->name}}</td>
			<td>{{ $user->email}}</td>
			<td>{{ $roles[ $user->role ] }}</td>
			<td>{{ $user->is_active }}</td>
			<td>{{ $user->id }}</td>
			<td>{{ $user->role }}</td>
			<td>{{ $user->avatar }}</td>
		</tr>
	<?php $count++; ?>
	@endforeach
	</tbody>
</table>

<!-- Popup edit -->
<div id="edit_popup" class="custom_popup w30p mfp-hide">
  <div class="box-body">
      <!-- <button class='close_btn btn btn-app fr'><i class="fa fa-times"></i> Đóng</button> -->

      <div class="row">
      	<div class="col-md-6 text-c">
      		<img src="/images/avatar/no-avatar.png" id="avatar-preview" class="w150">
      	</div>
      	<div class="col-md-6">
      		<form class="form-horizontal" id="edit_form">
          {{ csrf_field() }}
      			
      			<div class="form-group">
              <div class="col-md-12">
                <input id="popup_name" type="text" class="form-control noradius" name="name" value="" required autofocus>  
              </div>
	          </div>

	          <div class="form-group">
              <div class="col-md-12">
                <input id="popup_email" type="text" class="form-control noradius" name="email" value="" required autofocus>  
              </div>
	          </div>

	          <div class="form-group">
              <div class="col-md-12">
                  {{ Form::select('role', $roles, null , ['class'=>'form-control noradius','id'=>'popup_role']) }}
              </div>
            </div>

            <input type="hidden" name="id" id="popup_id">
  				</form>
      	</div>
      </div>

      <button class='close_btn btn btn-app fr pb40 noradius'><i class="fa fa-times"></i> Close</button>
      <button class='btn btn-app fr pb40 mr20 noradius' id="update_btn"><i class="fa fa-save"></i> Update</button>
  </div>
  <!-- /.box-body -->
</div>


@section('page-script')
<script src="{{ asset('js/dropzone.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function()
	{
    //Init the datatable
    $('#users-list').DataTable({
    	"info": true,
    	"aoColumnDefs": [
    		{ "bVisible": false, "aTargets": [-1,-2] },
    		{ className: "dt-center", "targets": [3,4] },
    		{
          targets: -4,
          render: function(row, type, val, meta) {
          	return ((val[4] != 0)?"Active":"Blocked");
          },
        },
    		{
          targets: -3,
          render: function(row, type, val, meta) {
          	var blockBtn = '<a href="" class="block_btn" data-id="'+val[0]+'" data-type="block">Block</a>';
          	if( val[4] != 1 )
          	{
          		blockBtn = '<a href="" class="block_btn" data-id="'+val[0]+'" data-type="unblock">Unblock</a>'
          	}

            return '<a href="#edit_popup" \
            					 class="edit_btn" \
            					 data-id="'+val[0]+'" \
            					 data-name="'+val[1]+'" \
            					 data-email="'+val[2]+'" \
            					 data-role="'+val[5]+'" \
            					 data-avatar="'+val[7]+'" \
            					 >Edit</a> |\
            			  '+blockBtn+' |\
            			  <a href="" class="delete_btn" data-id="'+val[0]+'">Delete</a>';
          },
        },
    	],
    }); //Datatable

    //Initialize the magnific popup EDIT
	  $(function () {
	    $('.edit_btn').magnificPopup({
	      //type: 'inline',
	      preloader: false,
	      focus: '#edit_popup',
	      closeBtnInside:true,
	      modal: true,
	      callbacks:{
	      	open: function(){
	      		resetPopup( $('#edit_form') );
	      		//Get button element when click popup
	      		var mp = $.magnificPopup.instance;
	      		curItem = $(mp.currItem.el[0]);
	      		$("#popup_name").val( curItem.data('name') );
	      		$("#popup_email").val( curItem.data('email') );
	      		$("#popup_role").val( curItem.data('role') );
	      		$("#popup_id").val( curItem.data('id') );
	      		var url = "{{$url}}";
	      		(curItem.data('avatar') != "" ? $("#avatar-preview").attr('src',url+curItem.data('avatar')) : "" )
	      	}
	      }
	    });
	    $(document).on('click', '.close_btn', function (e) {
	      e.preventDefault();
	      $.magnificPopup.close();
	    });
	    $("#update_btn").on("click",function(e){
	    	e.preventDefault();
	    	if( confirm("Are you sure to update this data") )
	    	{
	    		var data = {"id" : $("#popup_id").val(),
							"name" : $("#popup_name").val(),
							"email" : $("#popup_email").val(),
							"role" : $("#popup_role").val(),
	    		};
	    		//Call ajax
	    		var result = callAjax( "update" , data );
	    		if( result != false )
	    		{
	    			toastr.success('User information has been updated', 'Success');
	    			setTimeout(function(){ 
	    				location.reload();
	    			}, 1000);
	    		}
	    	}
	    	return false;
	    });
	  });

	  //Block the user
	  $(document).on('click', '.block_btn', function (e) {
      e.preventDefault();
      if( confirm("Are you sure to block/unblock this user") )
    	{
    		var data = {"id" : $(this).attr("data-id"),"type":$(this).attr("data-type")};
    		//Call ajax
    		var result = callAjax( "block" , data );
    		if( result != false )
    		{
    			location.reload();
    		}
    	}
    	return false;
    });

    //Delete the user
	  $(document).on('click', '.delete_btn', function (e) {
      e.preventDefault();
      if( confirm("Are you sure to delete this user") )
    	{
    		var data = {"id" : $(this).attr("data-id")};
    		//Call ajax
    		var result = callAjax( "delete" , data );
    		if( result != false )
    		{
    			location.reload();
    		}
    	}
    	return false;
    });

	}); //Ready

	//Reset all data from popup
	function resetPopup(ele)
	{
		ele.trigger("reset");
	}

	//Ajax function
	function callAjax(action,data)
	{
		var flag;
		$.ajax({
	    type: "POST",
	    url: "/admin/ajax-user",
	    dataType: 'json',
	    data: {"_token": "{{ csrf_token() }}",
	    			 "action": action,
	    			 "data": data
	    			},
	    success: function(data){
	    	if( data.response != true )
	    	{
	    		flag = false;
	    	}
	    	else
	    	{
	    		flag = true;
	    	}
	    },
	  });
	  return flag;
	}

</script>
@stop
@endsection