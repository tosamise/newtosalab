@extends('layouts.backend')

@section('content')
<?php $url = "/images/camera/"; ?>
<h3 class="page-header hidden-xs ">CAMERA LIST</h3>

<div class="row mb20">
  <div class="col-md-12">
    <input id="search_box" class="form-control w200 fl noradius mr20" placeholder="Type to search">
    <a href="#add_popup" class='btn btn-default fl noradius' id="add_btn"><i class="fa fa-plus"></i> Add new</a>
  </div>
</div>

<table id="camera-list" class="table table-default">
	<thead>
		<tr>
			<th>No.</th>
			<th>Preview</th>
			<th>Name</th>
			<th>Type</th>
			<th>Lens</th>
			<th>Price</th>
			<th>Belong to</th>
			<th>Status</th>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
	<?php $count = 1; ?>
	@foreach( $cameras as $camera )
		<tr>
			<td>{{ $count }}</td>
			<td>{{ $camera->cam_image}}</td>
			<td>{{ $camera->cam_name}}</td>
			<td>{{ $camera->cam_type}}</td>
			<td>{{ $camera->lens}}</td>
			<td>{{ $camera->bought_price}}</td>
			<td>{{ $camera->belong_to}}</td>
			<td>{{ $camera->is_sold}}</td>
			<td>{{ $camera->id}}</td>
			<td>{{ $camera->cam_series}}</td>
		</tr>
	<?php $count++; ?>
	@endforeach
	</tbody>
</table>

<!-- Popup edit -->
<div id="edit_popup" class="custom_popup w50p mfp-hide">
  <div class="box-body">
    <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
      @endforeach
    </div> <!-- end .flash-message -->
    {{ Form::model( null, ['url' => ['/admin/info/camera/store'], 'method' => 'post', 'role' => 'form', 'id' => 'edit_form', 'class' => 'form-horizontal', 'files' => true] ) }}
          {{ csrf_field() }}
      <div class="row">
      	<div class="col-md-4 text-c">
      		<img src="/images/camera/no-camera.jpg" id="camera-preview" class="w150">
          
          {{ Form::file('cam_image', ['class'=>'form-control noradius mt20','id'=>'popup_upload']) }}
          @if ($errors->has('cam_image'))
            <span class="help-block">
              <strong>{{ $errors->first('cam_image') }}</strong>
            </span>
          @endif
      	</div>
      	<div class="col-md-8">
          {{ csrf_field() }}
      			
      			<div class="form-group">
              <div class="col-md-12">
                {{ Form::text('cam_name', null, ['class'=>'form-control noradius','id'=>'popup_name','placeholder'=>'camera name']) }}

                @if ($errors->has('cam_name'))
                  <span class="help-block">
                    <strong>{{ $errors->first('cam_name') }}</strong>
                  </span>
                @endif
              </div>
	          </div>

	          <div class="form-group">
              <div class="col-md-12">
                {{ Form::select('cam_type', Config::get('info.cam_type') ,null, ['class'=>'form-control noradius','id'=>'popup_type']) }}
              </div>
	          </div>

	          <div class="form-group">
              <div class="col-md-12">
                {{ Form::text('lens', null, ['class'=>'form-control noradius','id'=>'popup_lens','placeholder'=>'lens info']) }}

                @if ($errors->has('lens'))
                  <span class="help-block">
                    <strong>{{ $errors->first('lens') }}</strong>
                  </span>
                @endif
              </div>
	          </div>

	          <div class="form-group">
              <div class="col-md-12">
                {{ Form::text('cam_series', null, ['class'=>'form-control noradius','id'=>'popup_series','placeholder'=>'series']) }}

                @if ($errors->has('cam_series'))
                  <span class="help-block">
                    <strong>{{ $errors->first('cam_series') }}</strong>
                  </span>
                @endif
              </div>
	          </div>

	          <div class="form-group">
              <div class="col-md-12">
                {{ Form::text('bought_price', null, ['class'=>'form-control noradius','id'=>'popup_price','placeholder'=>'price']) }}

                @if ($errors->has('bought_price'))
                  <span class="help-block">
                    <strong>{{ $errors->first('bought_price') }}</strong>
                  </span>
                @endif
              </div>
	          </div>

	          <div class="form-group">
              <div class="col-md-12">
                {{ Form::select('belong_to', $users ,null, ['class'=>'form-control','id'=>'popup_belongto']) }}
              </div>
	          </div>

            {{ Form::hidden('id', null, ['id'=>'popup_id']) }}
            {{ Form::hidden('flag', 'edit', ['value'=>'']) }}
  				
      	</div>
      </div>
    {{ Form::close() }}
      
    <button class='close_btn btn btn-default fr pb40 noradius'><i class="fa fa-times"></i> Close</button>
    <button class='btn btn-default fr pb40 mr20 noradius' id="update_btn"><i class="fa fa-save"></i> Update</button>

    <button class='btn btn-danger fl pb40 mr20 noradius' id="sold_btn" data-id="" ><i class="fa fa-cart-arrow-down"></i> Sold</button>
  </div>
  <!-- /.box-body -->
</div>

<!-- Popup add -->
<div id="add_popup" class="custom_popup w50p mfp-hide">
  <div class="box-body">
    {{ Form::model( null, ['url' => ['/admin/info/camera/store'], 'method' => 'post', 'role' => 'form', 'id' => 'add_form', 'class' => 'form-horizontal', 'files' => true] ) }}
          {{ csrf_field() }}
      <div class="row">
      	<div class="col-md-4 text-c">
      		<img src="/images/camera/no-camera.jpg" id="camera-preview" class="w150">
          {{ Form::file('cam_image', ['class'=>'form-control noradius mt20','id'=>'popup_upload']) }}
          @if ($errors->has('cam_image'))
            <span class="help-block">
              <strong>{{ $errors->first('cam_image') }}</strong>
            </span>
          @endif
      		<p id="popup_status" class="red-text"></p>
      	</div>
      	<div class="col-md-8">
      		
      			
      			<div class="form-group">
              <div class="col-md-12">
                {{ Form::text('cam_name', null, ['class'=>'form-control noradius','id'=>'popup_name','placeholder'=>'camera name']) }}

                @if ($errors->has('cam_name'))
                  <span class="help-block">
                    <strong>{{ $errors->first('cam_name') }}</strong>
                  </span>
                @endif
              </div>
	          </div>

	          <div class="form-group">
              <div class="col-md-12">
                {{ Form::select('cam_type', Config::get('info.cam_type') ,null, ['class'=>'form-control noradius']) }}
              </div>
	          </div>

	          <div class="form-group">
              <div class="col-md-12">
                {{ Form::text('lens', null, ['class'=>'form-control noradius','id'=>'popup_lens','placeholder'=>'lens info']) }}

                @if ($errors->has('lens'))
                  <span class="help-block">
                    <strong>{{ $errors->first('lens') }}</strong>
                  </span>
                @endif
              </div>
	          </div>

	          <div class="form-group">
              <div class="col-md-12">
                {{ Form::text('cam_series', null, ['class'=>'form-control noradius','id'=>'popup_series','placeholder'=>'series']) }}

                @if ($errors->has('cam_series'))
                  <span class="help-block">
                    <strong>{{ $errors->first('cam_series') }}</strong>
                  </span>
                @endif
              </div>
	          </div>

	          <div class="form-group">
              <div class="col-md-12">
                {{ Form::text('bought_price', null, ['class'=>'form-control noradius','id'=>'popup_price','placeholder'=>'price']) }}

                @if ($errors->has('bought_price'))
                  <span class="help-block">
                    <strong>{{ $errors->first('bought_price') }}</strong>
                  </span>
                @endif 
              </div>
	          </div>

	          <div class="form-group">
              <div class="col-md-12">
                {{ Form::select('belong_to', $users ,null, ['class'=>'form-control','id'=>'popup_belongto']) }}  
              </div>
	          </div>

            {{ Form::hidden('flag', 'add', ['value'=>'']) }}

      	</div>
      </div>
    {{ Form::close() }}

    <button class='close_btn btn btn-default fr pb40 noradius'><i class="fa fa-times"></i> Close</button>
    <button class='btn btn-default fr pb40 mr20 noradius' id="submit_btn"><i class="fa fa-save"></i> Save</button>
  </div>
  <!-- /.box-body -->
</div>


@section('page-script')
<script src="{{ asset('js/jquery.number.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function()
	{
    //Init the datatable
    var table = $('#camera-list').DataTable({
        "pageLength": 10,
        "info": true,
      	"bLengthChange": false, //Hide select box
        "aoColumnDefs": [
    		{ "bVisible": false, "aTargets": [-1,-2] },
    		{
          targets: 0,
        	className: "dt-center",
        	width: '1%'
        },
    		{
          targets: 1,
          className: "dt-center",
          render: function(row, type, val, meta) {
          	var url = "{{$url}}";
          	// console.log(val);
          	return "<a href='#edit_popup' \
            					 class='edit_btn' \
            					 data-id='"+val[8]+"'\
            					 data-image='"+val[1]+"'\
            					 data-name='"+val[2]+"'\
            					 data-type='"+val[3]+"'\
            					 data-lens='"+val[4]+"'\
            					 data-price='"+val[5]+"'\
            					 data-belongto='"+val[6]+"'\
            					 data-series='"+val[9]+"'\
            					 data-status='"+val[7]+"'\
            					 > \
            					<img src='"+url+val[1]+"' width='200px' title='"+val[2]+"' />\
            				</a>";
          },
        },
        {
          targets: 2,
        	className: "dt-center"
        },
        {
          targets: 3,
        	className: "dt-center",
          render: function(row, type, val, meta) {
            var cam_type = <?php echo json_encode( Config::get('info.cam_type') ) ?>;
            return cam_type[val[3]];
          }
        },
        {
          targets: 4,
        	className: "dt-center"
        },
        {
          targets: 5,
        	className: "dt-right",
        	render: function(row, type, val, meta) {
            return $.number( val[5] , 0 , ',' );
          }
        },
        {
          targets: 6,
        	className: "dt-center",
        	render: function(row, type, val, meta) {
          	var users = <?php echo json_encode($users); ?>;
          	return users[val[6]];
          },
        },
        {
          targets: 7,
          className: "dt-center",
          render: function(row, type, val, meta) {
          	if(val[7] != 0){ return "Sold"}else{ return "Using"};
          },
        },
    	],
    }); //Datatable

    //Action when search is input.
    $("#search_box").keyup(function(){
      table.search($(this).val()).draw() ;
    })

    //Initialize the magnific popup EDIT
    $(function () {
      $('#add_btn').magnificPopup({
        //type: 'inline',
        preloader: false,
        focus: '#add_popup',
        closeBtnInside:true,
        modal: true,
        callbacks:{
          beforeOpen: function() {
            resetPopup( $('#add_form') );
            $("#add_popup #camera-preview").attr("src","/images/camera/no-camera.jpg");
            //Debug
            $("#add_popup #popup_name").val("Olympus 35 DC");
            $("#add_popup #popup_type").val("rf");
            $("#add_popup #popup_lens").val("40mm f1.7");
            $("#add_popup #popup_series").val("141kfa928");
          },
          open: function(){
            
          }
        }
      });
      $(document).on('click', '.close_btn', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
      });
      $("#submit_btn").on("click",function(e){
        e.preventDefault();
        if( confirm("Are you sure to add this data") )
        {
          $("#add_form").submit();
        }
        return false;
      });
    });

    //Initialize the magnific popup EDIT
	  $(function () {
	    $('.edit_btn').magnificPopup({
	      //type: 'inline',
	      preloader: false,
	      focus: '#edit_popup',
	      closeBtnInside:true,
	      modal: true,
	      callbacks:{
			    beforeOpen: function() {
			    	$("#popup_status").text("");
			    	resetPopup( $('#edit_form') );
			    },
	      	open: function(){
	      		//Get button element when click popup
	      		var mp = $.magnificPopup.instance;
	      		curItem = $(mp.currItem.el[0]);
	      		$("#edit_popup #popup_name").val( curItem.data('name') );
	      		$("#edit_popup #popup_type").val( curItem.data('type') );
	      		$("#edit_popup #popup_lens").val( curItem.data('lens') );
	      		$("#edit_popup #popup_price").val( $.number( curItem.data('price') , 0 , ',' ) );
	      		$("#edit_popup #popup_series").val( curItem.data('series') );
	      		$("#edit_popup #popup_belongto").val( curItem.data('belongto') );
	      		$("#edit_popup #popup_id").val( curItem.data('id') );
	      		if( curItem.data('status') != 1 )
	      		{
	      			$("#sold_btn").attr( "data-id", curItem.data('id') );
	      		}
	      		else
	      		{
	      			$("#sold_btn").hide();
	      			$("#edit_popup #popup_status").text("Sold");
	      		}

	      		var url = "{{$url}}";
	      		(curItem.data('image') != "" ? $("#edit_popup #camera-preview").attr('src',url+curItem.data('image')) : "" )
	      	}
	      }
	    });
	    $(document).on('click', '.close_btn', function (e) {
	      e.preventDefault();
	      $.magnificPopup.close();
	    });
	    $("#update_btn").on("click",function(e){
	    	e.preventDefault();
	    	if( confirm("Are you sure to update this data") )
	    	{
	    		$("#edit_form").submit();
	    	}
	    	return false;
	    });
	  });

    //Sold the camera
	  $(document).on('click', '#sold_btn', function (e) {
      e.preventDefault();
      if( confirm("Are you sure to mark as sold") )
    	{
    		var data = {"id" : $(this).attr("data-id")};
    		//Call ajax
    		var result = callAjax( "sold", "camera" , data );
    		if( result != false )
    		{
          window.location.replace("/admin/info/camera");
    			//location.reload();
    		}
    	}
    	return false;
    });

	}); //Ready

	// //Ajax function
	function callAjax(action,kind,data)
	{
		var flag;
		$.ajax({
	    type: "POST",
	    url: "/admin/ajax-info",
	    dataType: 'json',
	    data: {"_token": "{{ csrf_token() }}",
	    			 "action": action,
	    			 "kind": kind,
	    			 "data": data
	    			},
	    success: function(data){
	    	if( data.response != true )
	    	{
	    		flag = false;
	    	}
	    	else
	    	{
	    		flag = true;
	    	}
	    },
	  });
	  return flag;
	}

	//Format price when typing
	$("#edit_popup  #popup_price").on("keyup change", function(){
    $(this).val( $.number( $(this).val() , 0, ',' ) );  
  });
  $("#add_popup  #popup_price").on("keyup change", function(){
    $(this).val( $.number( $(this).val() , 0, ',' ) );  
  });
  
  //Preview image before upload
  //Add popup
  $("#add_popup #popup_upload").change(function(){
      readURL(this,'#add_popup #camera-preview');
  });
  //Edit popup
  $("#edit_popup #popup_upload").change(function(){
      readURL(this,'#edit_popup #camera-preview');
  });



</script>
@stop
@endsection