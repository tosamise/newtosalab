@extends('layouts.backend')

@section('content')
<h3 class="page-header hidden-xs ">ROLL LIST</h3>

<div class="row mb20">
<div class="col-md-12">
    <input id="search_box" class="form-control w200 fl noradius mr20" placeholder="Type to search">
    <a href="#add_popup" class='btn btn-default fl noradius' id="add_btn"><i class="fa fa-plus"></i> Add new</a>
</div>
</div>

<table id="roll-list" class="table table-default">
<thead>
<tr>
    <th>No.</th>
    <th>Code</th>
    <th>Lab</th>
    <th>Camera</th>
    <th>Film</th>
    <th>Created date</th>
    <th>Last edit</th>
    <th></th>
</tr>
</thead>
<tbody>
<?php $count = 1; ?>
@foreach( $rolls as $roll )
    <tr>
        <td>{{ $count }}</td>
        <td>{{ $roll->code_film}}</td>
        <td>{{ $roll->lab_id}}</td>
        <td>{{ $roll->camera_id}}</td>
        <td>{{ $roll->film_id}}</td>
        <td>{{ $roll->created_at}}</td>
        <td>{{ $roll->updated_at}}</td>
        <td>{{ $roll->id}}</td>
    </tr>
    <?php $count++; ?>
@endforeach
</tbody>
</table>

<!-- Popup add -->
<div id="add_popup" class="custom_popup w50p mfp-hide">
    <div class="box-body">
        {{ Form::model( null, ['url' => ['/admin/info/roll/store'], 'method' => 'post', 'role' => 'form', 'id' => 'add_form', 'class' => 'form-horizontal', 'files' => true] ) }}
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-4 text-c">
                <img src="/images/lab/no-lab.jpg" id="lab-preview" class="w150 mb5">
                <img src="/images/camera/no-camera.jpg" id="camera-preview" class="w150 mb5">
                <img src="/images/film/no-film.jpg" id="film-preview" class="w150 mb5">
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <div class="col-md-12">
                        {{ Form::text('code_film', old('code_film'), ['class'=>'form-control noradius','id'=>'popup_code','placeholder'=>'Roll code: 154578']) }}

                        @if ($errors->has('code_film'))
                            <span class="help-block">
                                <strong>{{ $errors->first('code_film') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        {{ Form::select('lab_id', $labs, old('lab_id'), ['class'=>' noradius','id'=>'popup_lab','placeholder'=>'Please select lab']) }}

                        @if ($errors->has('lab_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('lab_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        {{ Form::select('camera_id', $cameras, old('camera_id'), ['class'=>'noradius','id'=>'popup_camera','placeholder'=>'Please select camera']) }}

                        @if ($errors->has('camera_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('camera_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        {{ Form::select('film_id', $films, old('film_id'), ['class'=>'noradius','id'=>'popup_film','placeholder'=>'Please select film type']) }}

                        @if ($errors->has('film_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('film_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                {{ Form::hidden('flag', 'add', ['value'=>'']) }}

            </div>
        </div>
        {{ Form::close() }}

        <button class='close_btn btn btn-default fr pb40 noradius'><i class="fa fa-times"></i> Close</button>
        <button class='btn btn-default fr pb40 mr20 noradius' id="submit_btn"><i class="fa fa-save"></i> Save</button>
    </div>
    <!-- /.box-body -->
</div>


@section('page-script')
<script>
//Init variables
var film_url = "/images/film/";
var camera_url = "/images/camera/";
var lab_url = "/images/lab/";

$(document).ready(function(){
    //Open popup again if validation error
    var error_flag = "<?php echo old('flag') ?>";

    if( error_flag )
    {
        if( error_flag == 'add' )
        {
            $.magnificPopup.open({
                items: {
                    src: '#add_popup'
                },
                type: 'inline'
            });
        }

        if( error_flag == 'edit' )
        {
            $.magnificPopup.open({
                items: {
                    src: '#edit_popup'
                },
                type: 'inline'
            });
        }
    }

    //Validation by Jquery
    var validator = $( "#add_form" ).validate({
        onfocusout: function (element) {
            $(element).valid();
        },
        rules: {
            code_film: {
                required: true,
                remote: {
                    url: "/admin/ajax-info",
                    type: "post",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "action": "checkCodeFilm",
                        "kind": "roll",
                    },
                }
            }
        },
        messages:{
            code_film: {
                required: "Please insert this field",
                remote: "This code film was already existed"
            }
        },
        highlight: function(element) {
            $(element).parent().addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).parent().removeClass('has-error');
        },
    });

    //Init the selectize for all selectbox
    $(function() {
        $('select').selectize();
    });

    //Init the datatable
    var table = $('#roll-list').DataTable({
        "pageLength": 10,
        "info": true,
        "bLengthChange": false, //Hide select box
        "aoColumnDefs": [
            { "bVisible": false, "aTargets": [-1] },
            {
                targets: "_all",
                className: "dt-center",
            },
            {
                targets: 0,
                width: '1%'
            },
            {
                targets: 1,
                render: function(row, type, val, meta) {
                    // console.log(val);
                    return "<a href='#edit_popup' \
        					 class='edit_btn' \
        					 data-id='"+val[7]+"'\
                             data-lab='"+val[2]+"'\
                             data-camera='"+val[3]+"'\
                             data-film='"+val[4]+"'>\
        					"+val[1]+"\
        				</a>";
                },
            },
            {
                //Lab
                targets: [2],
                render: function(row, type, val, meta) {
                    var labs = <?php echo json_encode($labs); ?>;
                    return labs[val[2]];
                },
            },
            {
                //Camera
                targets: [3],
                render: function(row, type, val, meta) {
                    var cameras = <?php echo json_encode($cameras); ?>;
                    return cameras[val[3]];
                },
            },
            {
                //Film
                targets: [4],
                render: function(row, type, val, meta) {
                    var films = <?php echo json_encode($films); ?>;
                    return films[val[4]];
                },
            },
            {
                //Create date
                targets: [5],
                render: function(row, type, val, meta) {
                    return formatDate(val[5]);
                },
            },
            {
                //Last update
                targets: [6],
                render: function(row, type, val, meta) {
                    return formatDate(val[6]);
                },
            },
        ],
    }); //Datatable

    //Action when search is input.
    $("#search_box").keyup(function(){
        table.search($(this).val()).draw() ;
    })

    //Initialize the magnific popup EDIT
    $(function () {
        $('#add_btn').magnificPopup({
            //type: 'inline',
            preloader: false,
            focus: '#add_popup',
            closeBtnInside:true,
            modal: true,
            callbacks:{
                beforeOpen: function() {
                    resetPopup( $('#add_form') );
                },
                open: function(){

                }
            }
        });
        $(document).on('click', '.close_btn', function (e) {
            e.preventDefault();
            $.magnificPopup.close();
        });
        $("#submit_btn").on("click",function(e){
            e.preventDefault();
            if( confirm("Are you sure to add this data") )
            {
                $("#add_form").submit();
            }
            return false;
        });
    });

    //Initialize the magnific popup EDIT
    $(function () {
        $('.edit_btn').magnificPopup({
            //type: 'inline',
            preloader: false,
            focus: '#edit_popup',
            closeBtnInside:true,
            modal: true,
            callbacks:{
                beforeOpen: function() {
                    $("#popup_status").text("");
                    resetPopup( $('#edit_form') );
                },
                open: function(){
                    //Get button element when click popup
                    var mp = $.magnificPopup.instance;
                    curItem = $(mp.currItem.el[0]);
                    $("#edit_popup #popup_model").val( curItem.data('model') );
                    $("#edit_popup #popup_type").val( curItem.data('type') );
                    $("#edit_popup #popup_name").val( curItem.data('name') );
                    $("#edit_popup #popup_exp").val( curItem.data('exp') );
                    $("#edit_popup #popup_iso").val( curItem.data('iso') );
                    $("#edit_popup #popup_madein").val( curItem.data('madein') );
                    $("#edit_popup #popup_id").val( curItem.data('id') );

                    if( curItem.data('status') != 1 )
                    {
                        $("#discont_btn").attr( "data-id", curItem.data('id') );
                    }
                    else
                    {
                        $("#discont_btn").hide();
                        $("#edit_popup #popup_status").text("Discontinued");
                    }
                }
            }
        });
        $(document).on('click', '.close_btn', function (e) {
            e.preventDefault();
            $.magnificPopup.close();
        });
        $("#update_btn").on("click",function(e){
            e.preventDefault();
            if( confirm("Are you sure to update this data") )
            {
                $("#edit_form").submit();
            }
            return false;
        });
    });

    //Load preview image when selectbox being chosen
    $("#popup_lab").val()?getLabImage( $("#popup_lab") ):"";
    $("#popup_film").val()?getFilmImage( $("#popup_film") ):"";
    $("#popup_camera").val()?getCameraImage( $("#popup_camera") ):"";

});//end ready

//Delete the roll
$(document).on('click', '#del_btn', function (e) {
    e.preventDefault();
    if( confirm("Are you sure to delete this roll record") )
    {
        var data = {"id" : $(this).attr("data-id")};
        //Call ajax
        var result = callAjax( "delete", "roll" , data );
        if( result != false )
        {
            window.location.replace("/admin/info/roll");
        }
    }
    return false;
});

//Handle the selectbox on choose
$(document).on('change', '#popup_lab', function (e) {
    getLabImage( $(this) );
});
$(document).on('change', '#popup_camera', function (e) {
    getCameraImage( $(this) );
});
$(document).on('change', '#popup_film', function (e) {
    getFilmImage( $(this) );
});

//Detail functions
function getFilmImage(ele)
{
    var src = getData( "getImage" , "film" , ele.val() );
    $("#film-preview").attr("src",film_url+src['film_image']);
}

function getCameraImage(ele)
{
    var src = getData( "getImage" , "camera" , ele.val() );
    $("#camera-preview").attr("src",camera_url+src['cam_image']);
}

function getLabImage(ele)
{
    var src = getData( "getImage" , "lab" , ele.val() );
    $("#lab-preview").attr("src",lab_url+src['lab_image']);
}

//Ajax function
function callAjax(action,kind,data)
{
    var flag;
    $.ajax({
        type: "POST",
        url: "/admin/ajax-info",
        dataType: 'json',
        data: {"_token": "{{ csrf_token() }}",
                     "action": action,
                     "kind": kind,
                     "data": data
                    },
        success: function(data){
            if( data.response != true )
            {
                flag = false;
            }
            else
            {
                flag = true;
            }
        },
    });
    return flag;
}

//Get data by Ajax function
function getData(action,kind,data)
{
    var result;
    $.ajax({
        async: false,
        type: "POST",
        url: "/admin/ajax-info",
        dataType: 'json',
        data: {"_token": "{{ csrf_token() }}",
               "action": action,
               "kind": kind,
               "data": data
            },
        success: function(data){
            result = data[0];
        },
    });
    return result;
}

</script>
@stop
@endsection