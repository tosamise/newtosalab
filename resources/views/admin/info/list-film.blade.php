@extends('layouts.backend')

@section('content')
<?php $url = "/images/film/"; ?>
<h3 class="page-header hidden-xs ">FILM LIST</h3>

<div class="row mb20">
<div class="col-md-12">
    <input id="search_box" class="form-control w200 fl noradius mr20" placeholder="Type to search">
    <a href="#add_popup" class='btn btn-default fl noradius' id="add_btn"><i class="fa fa-plus"></i> Add new</a>
</div>
</div>

<table id="film-list" class="table table-default">
<thead>
<tr>
    <th>No.</th>
    <th>Preview</th>
    <th>Model</th>
    <th>Type</th>
    <th>Name</th>
    <th>Exp.</th>
    <th>ISO</th>
    <th>Made in</th>
    <th>Status</th>
    <th></th>
</tr>
</thead>
<tbody>
<?php $count = 1; ?>
@foreach( $films as $film )
    <tr>
        <td>{{ $count }}</td>
        <td>{{ $film->film_image}}</td>
        <td>{{ $film->model}}</td>
        <td>{{ $film->film_type}}</td>
        <td>{{ $film->film_name}}</td>
        <td>{{ $film->exp}}</td>
        <td>{{ $film->iso}}</td>
        <td>{{ $film->made_in}}</td>
        <td>{{ $film->is_discontinue }}</td>
        <td>{{ $film->id}}</td>
    </tr>
    <?php $count++; ?>
@endforeach
</tbody>
</table>

<!-- Popup add -->
<div id="add_popup" class="custom_popup w50p mfp-hide">
    <div class="box-body">
        {{ Form::model( null, ['url' => ['/admin/info/film/store'], 'method' => 'post', 'role' => 'form', 'id' => 'add_form', 'class' => 'form-horizontal', 'files' => true] ) }}
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-4 text-c">
                <img src="/images/film/no-film.jpg" id="film-preview" class="w150">
                {{ Form::file('film_image', ['class'=>'form-control noradius mt20','id'=>'popup_upload']) }}
                @if ($errors->has('film_image'))
                    <span class="help-block">
                      <strong>{{ $errors->first('film_image') }}</strong>
                    </span>
                @endif
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <div class="col-md-12">
                        {{ Form::text('model', null, ['class'=>'form-control noradius','id'=>'popup_model','placeholder'=>'Film model: Fuji, Kodak']) }}

                        @if ($errors->has('model'))
                            <span class="help-block">
                                <strong>{{ $errors->first('model') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        {{ Form::text('film_type', null, ['class'=>'form-control noradius','id'=>'popup_type','placeholder'=>'Film type: Color 35mm']) }}

                        @if ($errors->has('film_type'))
                            <span class="help-block">
                                <strong>{{ $errors->first('film_type') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        {{ Form::text('film_name', null, ['class'=>'form-control noradius','id'=>'popup_name','placeholder'=>'Film name: Fuji 200']) }}

                        @if ($errors->has('film_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('film_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        {{ Form::select('exp', Config::get('info.film.exp') ,null, ['class'=>'form-control noradius','id'=>'popup_exp']) }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        {{ Form::select('iso', Config::get('info.film.iso') ,null, ['class'=>'form-control noradius','id'=>'popup_iso']) }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        {{ Form::text('made_in', null, ['class'=>'form-control noradius','id'=>'popup_madein','placeholder'=>'Made in: Thailand']) }}

                        @if ($errors->has('made_in'))
                            <span class="help-block">
                                <strong>{{ $errors->first('made_in') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                {{ Form::hidden('flag', 'add', ['value'=>'']) }}

            </div>
        </div>
        {{ Form::close() }}

        <button class='close_btn btn btn-default fr pb40 noradius'><i class="fa fa-times"></i> Close</button>
        <button class='btn btn-default fr pb40 mr20 noradius' id="submit_btn"><i class="fa fa-save"></i> Save</button>
    </div>
    <!-- /.box-body -->
</div>

<!-- Popup edit -->
<div id="edit_popup" class="custom_popup w50p mfp-hide">
    <div class="box-body">
        {{ Form::model( null, ['url' => ['/admin/info/film/store'], 'method' => 'post', 'role' => 'form', 'id' => 'edit_form', 'class' => 'form-horizontal', 'files' => true] ) }}
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-4 text-c">
                <img src="/images/film/no-film.jpg" id="film-preview" class="w150">
                {{ Form::file('film_image', ['class'=>'form-control noradius mt20','id'=>'popup_upload']) }}
                @if ($errors->has('film_image'))
                    <span class="help-block">
                      <strong>{{ $errors->first('film_image') }}</strong>
                    </span>
                @endif
                <p id="popup_status" class="red-text"></p>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <div class="col-md-12">
                        {{ Form::text('model', null, ['class'=>'form-control noradius','id'=>'popup_model','placeholder'=>'Film model: Fuji, Kodak']) }}

                        @if ($errors->has('model'))
                            <span class="help-block">
                                <strong>{{ $errors->first('model') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        {{ Form::text('film_type', null, ['class'=>'form-control noradius','id'=>'popup_type','placeholder'=>'Film type: Color 35mm']) }}

                        @if ($errors->has('film_type'))
                            <span class="help-block">
                                <strong>{{ $errors->first('film_type') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        {{ Form::text('film_name', null, ['class'=>'form-control noradius','id'=>'popup_name','placeholder'=>'Film name: Fuji 200']) }}

                        @if ($errors->has('film_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('film_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        {{ Form::select('exp', Config::get('info.film.exp') ,null, ['class'=>'form-control noradius','id'=>'popup_exp']) }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        {{ Form::select('iso', Config::get('info.film.iso') ,null, ['class'=>'form-control noradius','id'=>'popup_iso']) }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        {{ Form::text('made_in', null, ['class'=>'form-control noradius','id'=>'popup_madein','placeholder'=>'Made in: Thailand']) }}

                        @if ($errors->has('made_in'))
                            <span class="help-block">
                                <strong>{{ $errors->first('made_in') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                {{ Form::hidden('flag', 'edit', ['value'=>'']) }}
                {{ Form::hidden('id', null, ['id'=>'popup_id']) }}

            </div>
        </div>
        {{ Form::close() }}

        <button class='close_btn btn btn-default fr pb40 noradius'><i class="fa fa-times"></i> Close</button>
        <button class='btn btn-default fr pb40 mr20 noradius' id="update_btn"><i class="fa fa-save"></i> Update</button>

        <button class='btn btn-danger fl pb40 mr20 noradius' id="discont_btn"><i class="fa fa-st"op></i> Discontinued</button>
    </div>
    <!-- /.box-body -->
</div>


@section('page-script')
<script>
    $(document).ready(function(){
        //Init the datatable
        var table = $('#film-list').DataTable({
            "pageLength": 10,
            "info": true,
            "bLengthChange": false, //Hide select box
            "aoColumnDefs": [
                { "bVisible": false, "aTargets": [-1] },
                {
                    targets: "_all",
                    className: "dt-center",
                    width: '1%'
                },
                {
                    targets: 1,
                    render: function(row, type, val, meta) {
                        var url = "{{$url}}";
                        //console.log(val);
                        return "<a href='#edit_popup' \
            					 class='edit_btn' \
            					 data-id='"+val[9]+"'\
            					 data-model='"+val[2]+"'\
            					 data-type='"+val[3]+"'\
            					 data-name='"+val[4]+"'\
            					 data-exp='"+val[5]+"'\
            					 data-iso='"+val[6]+"'\
            					 data-madein='"+val[7]+"'\
            					 data-status='"+val[8]+"'\
            					 data-image='"+val[1]+"'\
            					 > \
            					<img src='"+url+val[1]+"' width='200px' title='"+val[2]+"' />\
            				</a>";
                    },
                },
                {
                    targets: -2,
                    render: function(row, type, val, meta) {
                        return ((val[8]=="0")?"Available":"Discontinued");
                    },
                },
            ],
        }); //Datatable

        //Action when search is input.
        $("#search_box").keyup(function(){
            table.search($(this).val()).draw() ;
        })

        //Initialize the magnific popup EDIT
        $(function () {
            $('#add_btn').magnificPopup({
                //type: 'inline',
                preloader: false,
                focus: '#add_popup',
                closeBtnInside:true,
                modal: true,
                callbacks:{
                    beforeOpen: function() {
                        resetPopup( $('#add_form') );
                        $("#add_popup #film-preview").attr("src","/images/film/no-film.jpg");
                        //Debug
                        $("#add_popup #popup_model").val("Fuji");
                        $("#add_popup #popup_name").val("Fuji 200");
                        $("#add_popup #popup_type").val("Color 35mm");
                        $("#add_popup #popup_madein").val("Thailand");
                        //Set default value
                        $("#add_popup #popup_exp").val("36");
                        $("#add_popup #popup_iso").val("100");
                    },
                    open: function(){

                    }
                }
            });
            $(document).on('click', '.close_btn', function (e) {
                e.preventDefault();
                $.magnificPopup.close();
            });
            $("#submit_btn").on("click",function(e){
                e.preventDefault();
                if( confirm("Are you sure to add this data") )
                {
                    $("#add_form").submit();
                }
                return false;
            });
        });

        //Initialize the magnific popup EDIT
        $(function () {
            $('.edit_btn').magnificPopup({
                //type: 'inline',
                preloader: false,
                focus: '#edit_popup',
                closeBtnInside:true,
                modal: true,
                callbacks:{
                    beforeOpen: function() {
                        $("#popup_status").text("");
                        resetPopup( $('#edit_form') );
                    },
                    open: function(){
                        //Get button element when click popup
                        var mp = $.magnificPopup.instance;
                        curItem = $(mp.currItem.el[0]);
                        $("#edit_popup #popup_model").val( curItem.data('model') );
                        $("#edit_popup #popup_type").val( curItem.data('type') );
                        $("#edit_popup #popup_name").val( curItem.data('name') );
                        $("#edit_popup #popup_exp").val( curItem.data('exp') );
                        $("#edit_popup #popup_iso").val( curItem.data('iso') );
                        $("#edit_popup #popup_madein").val( curItem.data('madein') );
                        $("#edit_popup #popup_id").val( curItem.data('id') );

                        if( curItem.data('status') != 1 )
                        {
                            $("#discont_btn").attr( "data-id", curItem.data('id') );
                        }
                        else
                        {
                            $("#discont_btn").hide();
                            $("#edit_popup #popup_status").text("Discontinued");
                        }

                        var url = "{{$url}}";
                        (curItem.data('image') != "" ? $("#edit_popup #film-preview").attr('src',url+curItem.data('image')) : "" )
                    }
                }
            });
            $(document).on('click', '.close_btn', function (e) {
                e.preventDefault();
                $.magnificPopup.close();
            });
            $("#update_btn").on("click",function(e){
                e.preventDefault();
                if( confirm("Are you sure to update this data") )
                {
                    $("#edit_form").submit();
                }
                return false;
            });
        });

    });//end ready

    //Discontinued the film
      $(document).on('click', '#discont_btn', function (e) {
        e.preventDefault();
        if( confirm("Are you sure to mark as discontinued") )
        {
            var data = {"id" : $(this).attr("data-id")};
            //Call ajax
            var result = callAjax( "discont", "film" , data );
            if( result != false )
            {
                window.location.replace("/admin/info/film");
            }
        }
        return false;
    });

    //Ajax function
    function callAjax(action,kind,data)
    {
        var flag;
        $.ajax({
        type: "POST",
        url: "/admin/ajax-info",
        dataType: 'json',
        data: {"_token": "{{ csrf_token() }}",
                     "action": action,
                     "kind": kind,
                     "data": data
                    },
        success: function(data){
            if( data.response != true )
            {
                flag = false;
            }
            else
            {
                flag = true;
            }
        },
      });
      return flag;
    }

    //Preview image before upload
    //Add popup
    $("#add_popup #popup_upload").change(function(){
        readURL(this,'#add_popup #film-preview');
    });
    //Edit popup
    $("#edit_popup #popup_upload").change(function(){
        readURL(this,'#edit_popup #film-preview');
    });
</script>
@stop
@endsection