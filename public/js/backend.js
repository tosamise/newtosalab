//Read upload image
function readURL(input, target) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(target).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

//Timeout flash messsage
setTimeout(function () {
    $('.flash-message').fadeOut('fast');
}, 3000); // <-- time in milliseconds

//Reset all data from popup
function resetPopup(ele) {
    ele.trigger("reset");
}

//Format date time for US format
function formatDate(input,getTime){
    var d = new Date(Date.parse(input.replace(/-/g, "/")));
    var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var date =  month[d.getMonth()]+ " " + d.getDate() + ", " + d.getFullYear();
    
    return date;  
};
