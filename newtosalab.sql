-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 29, 2017 at 06:37 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.0.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newtosalab`
--
CREATE DATABASE IF NOT EXISTS `newtosalab` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `newtosalab`;

-- --------------------------------------------------------

--
-- Table structure for table `cameras`
--

DROP TABLE IF EXISTS `cameras`;
CREATE TABLE `cameras` (
  `id` int(10) UNSIGNED NOT NULL,
  `cam_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cam_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lens` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cam_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bought_price` decimal(15,0) DEFAULT NULL,
  `belong_to` int(10) UNSIGNED DEFAULT NULL,
  `cam_series` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_sold` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cameras`
--

INSERT INTO `cameras` (`id`, `cam_name`, `cam_type`, `lens`, `cam_image`, `bought_price`, `belong_to`, `cam_series`, `created_at`, `updated_at`, `is_sold`) VALUES
(1, 'Canon FT', 'slr', '50mm f1.8', 'canon_ft.jpg', '2500000', 2, '45154233', '2017-04-14 04:02:27', '2017-05-09 06:39:18', 0),
(2, 'Contaflex 1980s', 'slr', '50mm f1.4', 'contaflex1980s.jpg', '3200000', 2, 'fageh3t3ggh', '2017-05-26 07:54:12', '2017-05-26 07:54:12', 0),
(3, 'Olympus 35 DC', 'rf', '40mm f1.7', 'olympus_35Dc.jpg', '1800000', 2, '141kfa928', '2017-05-26 07:54:44', '2017-05-26 08:54:56', 0),
(4, 'Roilleiflex', 'mf', '50mm f1.8', 'rolleiflex28.jpg', '7000000', 2, '141kfa928ffw', '2017-05-26 07:55:15', '2017-05-26 07:55:15', 0),
(5, 'Yashica Fx-3 Super 2000', 'slr', '50mm f1.8', 'yashica_fx3_super2000.jpg', '2200000', 2, 'ag33eu4e33535', '2017-05-26 07:57:09', '2017-05-26 07:57:31', 1);

-- --------------------------------------------------------

--
-- Table structure for table `films`
--

DROP TABLE IF EXISTS `films`;
CREATE TABLE `films` (
  `id` int(10) UNSIGNED NOT NULL,
  `model` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `film_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `film_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exp` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iso` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `film_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `made_in` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_discontinue` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `films`
--

INSERT INTO `films` (`id`, `model`, `film_type`, `film_name`, `exp`, `iso`, `film_image`, `made_in`, `created_at`, `updated_at`, `is_discontinue`) VALUES
(1, 'Fuji', 'Color 35mm', 'Fujicolor 200', '36', '200', 'fujicolor200_36.jpg', 'Japan', '2017-04-12 11:05:15', '2017-05-29 11:01:14', 0),
(2, 'Fuji', 'Color 35mm', 'Fujicolor 100 Nội địa Nhật', '36', '100', 'fujicolor_100_36_ja.jpg', 'Japan', '2017-05-29 16:24:04', '2017-05-29 16:24:04', 0),
(3, 'Fuji', 'Color 35mm', 'Fuji Superia X-Tra 400', '24', '400', 'fujisuperia_xtra_400_24.jpg', 'Japan', '2017-05-29 16:25:43', '2017-05-29 16:25:54', 0),
(4, 'Fuji', 'Color 35mm', 'Fuji Superia X-tra 800', '36', '800', 'fujisuperia_xtra_800_24.jpg', 'Japan', '2017-05-29 16:26:45', '2017-05-29 16:26:45', 0),
(5, 'Kodak', 'Color 35mm', 'Kodak Colorplus 200', '36', '200', 'kodak_colorplus_200_36.jpg', 'Japan', '2017-05-29 16:29:00', '2017-05-29 16:29:00', 0),
(6, 'Kodak', 'Color 35mm', 'Afga colorplus 200', '24', '200', 'afga_vistaplus_200_24.jpg', 'Japan', '2017-05-29 16:30:33', '2017-05-29 16:30:33', 0),
(7, 'ILFORD', 'Panchromatic B&W Negative', 'ILFORD PANF Plus 400', '36', '400', 'ilford_pan_bw_400_36.jpg', 'Germany', '2017-05-29 16:33:47', '2017-05-29 16:33:47', 0);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_small` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apature` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shutter` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `roll_id` int(10) UNSIGNED DEFAULT NULL,
  `taken_by` int(10) UNSIGNED DEFAULT NULL,
  `coordinates` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_delete` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `file_name`, `file_small`, `title`, `apature`, `shutter`, `roll_id`, `taken_by`, `coordinates`, `description`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, '000001.JPG', '000001_thumb.JPG', 'Chân dung', 'f/1.8', '1/4000', 1, 2, '12.24406, 109.18676', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 0, '2017-04-05 17:00:00', '2017-05-08 05:00:02'),
(2, '000002.JPG', '000002_thumb.JPG', 'Áo dài', '5.6', '1/200', 1, 1, '10.0356,105.7808', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 0, '2017-04-05 17:00:00', '2017-04-05 17:00:00'),
(3, '000003.JPG', '000003_thumb.JPG', 'Áo dài', '2.8', '1/500', 2, 2, '23.25018, 105.17967', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 0, '2017-04-05 17:00:00', '2017-05-04 21:36:45'),
(4, '000004.JPG', '000004_thumb.JPG', 'Hàng rong', 'f/5.6', '1/250', 1, 2, '10.2423,106.3720', 'これは日本語の内容です。\r\nĐây là nội dung tiếng Việt', 0, '2017-04-05 17:00:00', '2017-05-04 22:28:10');

-- --------------------------------------------------------

--
-- Table structure for table `labs`
--

DROP TABLE IF EXISTS `labs`;
CREATE TABLE `labs` (
  `id` int(10) UNSIGNED NOT NULL,
  `lab_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lab_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lab_website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lab_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coordinates` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_close` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `labs`
--

INSERT INTO `labs` (`id`, `lab_name`, `lab_address`, `lab_website`, `lab_image`, `coordinates`, `created_at`, `updated_at`, `is_close`) VALUES
(1, 'Crop Lab', '12 NCB', 'http://croplab.com.vn', NULL, NULL, '2017-04-14 04:03:17', '2017-04-14 04:03:20', 0);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(25, '2014_10_12_000000_create_users_table', 1),
(26, '2014_10_12_100000_create_password_resets_table', 1),
(27, '2017_04_03_095911_create_roles_table', 1),
(32, '2017_04_06_080949_create_images_table', 2),
(42, '2017_04_12_171144_create_rolls_table', 3),
(43, '2017_04_12_171643_create_cameras_table', 3),
(44, '2017_04_12_172318_create_labs_table', 3),
(45, '2017_04_12_172601_create_films_table', 3),
(46, '2017_05_09_112116_edit_price_camera_table', 4),
(47, '2017_05_29_171408_edit_film_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `m_roles`
--

DROP TABLE IF EXISTS `m_roles`;
CREATE TABLE `m_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `role` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `m_roles`
--

INSERT INTO `m_roles` (`id`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '2017-04-05 17:00:00', '2017-04-05 17:00:00'),
(2, 'Photographer', '2017-04-08 17:00:00', '2017-04-08 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rolls`
--

DROP TABLE IF EXISTS `rolls`;
CREATE TABLE `rolls` (
  `id` int(10) UNSIGNED NOT NULL,
  `code_film` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lab_id` int(10) UNSIGNED DEFAULT NULL,
  `camera_id` int(10) UNSIGNED DEFAULT NULL,
  `film_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_delete` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rolls`
--

INSERT INTO `rolls` (`id`, `code_film`, `lab_id`, `camera_id`, `film_id`, `created_at`, `updated_at`, `is_delete`) VALUES
(1, '415252', 1, 1, 1, '2017-04-14 04:01:12', '2017-04-14 04:01:14', 0),
(2, '51367', 1, 1, 1, '2017-04-27 17:00:00', '2017-04-27 17:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL,
  `is_delete` tinyint(4) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `email`, `role`, `avatar`, `is_active`, `is_delete`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$XpHLO8AxDVECpXQAe1LcweIvpHW8TSRs/I1aSy3kvVC8qobx1smnW', 'admin@local.com', '1', 'no-avatar.png', 1, 0, 'wS8x7naN5OpIzawI9FmFMUEEAdmEAFvygTKDKodPyxFX3zbGHwWlZxQAG7Y4', '2017-04-06 02:55:31', '2017-04-27 06:57:23'),
(2, 'huydo', '$2y$10$Qi3GmsKq.OZxxu.9f1.dfOQQOJWxBDqpIGiNdQCwWG56jO4m2Uivu', 'huydo@local.com', '2', 'avatar.png', 1, 0, 'qV4TSYcVpnfMizppLrLw83vrT5Ilx0dj5GVE484gOA6b1AomlZYqFEoVTsJf', '2017-04-08 22:05:48', '2017-04-09 08:21:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cameras`
--
ALTER TABLE `cameras`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `films`
--
ALTER TABLE `films`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labs`
--
ALTER TABLE `labs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_roles`
--
ALTER TABLE `m_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `m_roles_role_unique` (`role`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `rolls`
--
ALTER TABLE `rolls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cameras`
--
ALTER TABLE `cameras`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `films`
--
ALTER TABLE `films`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `labs`
--
ALTER TABLE `labs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `m_roles`
--
ALTER TABLE `m_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `rolls`
--
ALTER TABLE `rolls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
