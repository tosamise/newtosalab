<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Roll extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rolls';

    /**
     * Get all roll
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllRoll()
    {
    	return DB::table('rolls')->where('is_delete','!=','1')->get();
    }

    /**
     * Check the code film is existed
     *
     * @return \Illuminate\Http\Response
     */
    public function CheckCodeFilm($param)
    {
        if( !empty($param) )
        {    
            if( Roll::where( 'code_film','=',$param['code_film'] )->exists() ) 
            {
                return false;
            }
            return true;
        }
    }
}
