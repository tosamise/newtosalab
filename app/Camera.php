<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Camera extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cameras';

    /**
     * Get all camera
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllCamera()
    {
        return DB::table('cameras')->get();
    }

    /**
     * Get list camera
     *
     * @return \Illuminate\Http\Response
     */
    public function getListCamera()
    {
    	return Camera::all()->where('is_sold','!=', 1)->pluck('cam_name', 'id');
    }

    /**
     * Sold camera
     *
     * @return \Illuminate\Http\Response
     */
    public function soldCamera($param)
    {
        if( !empty($param) )
        {
            $data = $param['data'];
            $cam = Camera::find($data["id"]);
            if( !empty($cam) )
            {
                $cam->is_sold = 1;
                $cam->updated_at = Date("Y-m-d H:i:s");
                //Save the data
                if( $cam->save() )
                {
                    return true;
                }
                return false;
            }
            return false;
        }
    }
}
