<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'images';

    /**
     * Get image of information
     *
     * @return \Illuminate\Http\Response
     */
    public function getImage($param)
    {
        if( !empty($param) )
        {
            if( $param['kind'] == "camera" ) 
            {
                return DB::table('cameras')->select('cam_image')
                                         ->where('is_sold','!=','1')
                                         ->where('id','=',$param['data'])
                                         ->get();
            }
            elseif( $param['kind'] == "film" ) 
            {
                return DB::table('films')->select('film_image')
                                         ->where('is_discontinue','!=','1')
                                         ->where('id','=',$param['data'])
                                         ->get();
            }
            elseif( $param['kind'] == "lab" ) 
            {
                return DB::table('labs')->select('lab_image')
                                         ->where('is_close','!=','1')
                                         ->where('id','=',$param['data'])
                                         ->get();
            }
        }
    }
}
