<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Lab extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'labs';

    /**
     * Get list lab
     *
     * @return \Illuminate\Http\Response
     */
    public function getListLab()
    {
    	return Lab::all()->where('is_close','!=', 1)->pluck('lab_name', 'id');
    }
}
