<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Film extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'films';

    /**
     * Get all film
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllFilm()
    {
    	return DB::table('films')->get();
    }

    /**
     * Get list film
     *
     * @return \Illuminate\Http\Response
     */
    public function getListFilm()
    {
    	return Film::all()->where('is_discontinue','!=', 1)->pluck('film_name', 'id');
    }

    /**
     * Discontinued the film
     *
     * @return \Illuminate\Http\Response
     */
    public function discontFilm($param)
    {
        if( !empty($param) )
        {
            $data = $param['data'];
            $film = Film::find($data["id"]);
            if( !empty($film) )
            {
                $film->is_discontinue = 1;
                $film->updated_at = Date("Y-m-d H:i:s");
                //Save the data
                if( $film->save() )
                {
                    return true;
                }
                return false;
            }
            return false;
        }
    }
}
