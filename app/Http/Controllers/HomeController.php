<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.home');
    }

    public function ddFunction()
     {
        //Khai báo mảng dữ liệu
        $lists = array(
                'Language'=>[
                'PHP',
                'Javascript',
                'HTML',
                'NodeJS'
            ],
                'Framework'=>[
                'Laravel 5',
                'Yii',
                'Phalcon'
            ]
        );
        dd($lists); //Sử dụng hàm dd để kiểm tra dữ liệu mảng
        return view('welcome'); //Trả về view 'welcome'
    }
}
