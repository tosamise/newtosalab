<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Session;
use Redirect;
use App\Image;
use App\Camera;
use App\Roll;
use App\User;

class GalleryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['front','ajax']]);
    }

    /**
     * Show the application front page.
     *
     * @return \Illuminate\Http\Response
     */
    public function front()
    {
        $images = DB::table('images')->select('file_name',
                                              'file_small',
                                              'title',
                                              'apature',
                                              'shutter'
                                            )
                                     ->orderBy('created_at')
                                     ->take(50) //limit
                                     ->get();

        return view('front', [ 'images' => $images]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = DB::table('images')->select('file_name',
                                              'file_small',
                                              'title',
                                              'apature',
                                              'shutter',
                                              'id'
                                            )
                                     ->orderBy('created_at')
                                     ->take(20) //limit
                                     ->get();
        //Get list for selectbox
        $rolls = Roll::all()->pluck('code_film', 'id');

        return view('admin.gallery.index')->with( compact('images','rolls') );
    }

    /**
     * Edit the image info
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($image_id)
    {
        if ( !empty($image_id) )
        {
            //Get the request data
            $images = DB::table('images')->find($image_id);
            //Get list for selectbox
            $rolls = Roll::all()->pluck('code_film', 'id');
            $users = User::all()->where('id','!=', 1)->pluck('name', 'id');

            return view('admin.gallery.edit')->with( compact('images','rolls','users') );
        }
    }

    /**
     * Edit the image info
     *
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'title' => 'required',
            'apature' => 'required',
            'shutter' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        //process the login
        if ($validator->fails()) 
        {
            return Redirect::to('/admin/gallery/edit/'.$id)
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } 
        else 
        {
            // store
            $image = Image::find($id);
            $image->title = Input::get('title');
            $image->roll_id = Input::get('roll_id');
            $image->apature = Input::get('apature');
            $image->shutter = Input::get('shutter');
            $image->taken_by = Input::get('taken_by');
            $image->description = Input::get('description');
            $image->coordinates = Input::get('coordinates');
            $image->updated_at = date("Y-m-d h:i:s");
            $image->save();

            // redirect
            Session::flash('message', 'Successfully updated image!');
            return Redirect::to('/admin/gallery/edit/'.$id);
        }
    }

    /**
     * Do the ajax
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax(Request $request)
    {
        if ($request->isMethod('post'))
        {
            //Get the request data
            $data = $request->all();
            //Filter the actions
            //Get image detail
            if( $data['action'] == 'get' )
            {
                return response()->json( $this->__getImageDetail( $request->all() ) );   
            }
        }
        return response()->json(['response' => 'false' ]);   
    }

    /**
     * Get image detail
     *
     * @return \Illuminate\Http\Response
     */
    private function __getImageDetail($param)
    {
        if( !empty($param) )
        {
            return $data = DB::table('images')->select([
                                    "images.id",
                                    "images.file_name",
                                    "images.title",
                                    "images.apature",
                                    "images.shutter",
                                    "images.coordinates",
                                    "images.description",
                                    "users.name",
                                    "users.created_at",
                                    "users.avatar",
                                    "m_roles.role",
                                    "rolls.code_film",
                                    "cameras.cam_name",
                                    "cameras.lens",
                                    "labs.lab_name",
                                    "films.film_name",
                                ])
                                  ->leftJoin('users','images.taken_by','=','users.id')
                                  ->leftJoin('m_roles','users.role','=','m_roles.id')
                                  ->leftJoin('rolls','images.roll_id','=','rolls.id')
                                  ->leftJoin('cameras','rolls.camera_id','=','cameras.id')
                                  ->leftJoin('labs','rolls.lab_id','=','labs.id')
                                  ->leftJoin('films','rolls.film_id','=','films.id')
                                  ->where(['images.id'=>$param['id']])
                                  // ->toSql();
                                  ->get();
        }
    }

}
