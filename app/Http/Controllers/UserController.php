<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Http\Requests;
use App\User;
use App\Role;

class UserController extends Controller
{
    /**
     * Show the user list - Backend
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
    	$users = User::where(['is_delete'=>'0'])->get();
    	$roles = Role::all()->pluck('role', 'id');
        return view('admin.user.list-user')->with( compact('users','roles') );
    }

    /**
     * Show the user role list - Backend
     *
     * @return \Illuminate\Http\Response
     */
    public function showRole()
    {
        $roles = Role::all()->pluck('role', 'id');
        return view('admin.user.list-user')->with( 'roles' , $roles );
    }

    /**
     * Upload file - Backend
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        // if ($request->isMethod('post'))
        // {
        //     //Get the request data
        //     $data_file = $request->file('file');
        //     $data_info = $request->all();

        //     if (!is_dir( public_path().'/images/avatar/' ))
        //     {
        //       mkdir( public_path().'/images/avatar/', 0777 );
        //     }
            
        //     $fileName = $_FILES['file']['name'];
        //     $targetFile = $targetDir.$fileName;
        //     $MPhotos = TableRegistry::get('MPhotos');
        //     $photos = $MPhotos->newEntity();
        //     //print_r( $this->response );
        //     if(move_uploaded_file( $_FILES['file']['tmp_name'], TMP . '/' . $fileName ))
        //     {
        //       if($this->__resize_img(TMP, $targetDir, $fileName, 600))
        //       {
        //         $data['img_name'] = $fileName;
        //         $data['created'] = date('Y-m-d H:i:s');
        //         $data['modified'] = date('Y-m-d H:i:s');
        //         $photos = $MPhotos->patchEntity($photos, $data);
                
        //         if ( $MPhotos->save($photos) ) 
        //         {
        //           $this->response;
        //         } 
        //       } 
        //       unlink( TMP . '/' . $fileName);
        //     }
        // }
    }

    /**
     * Do the ajax
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax(Request $request)
    {
        if ($request->isMethod('post'))
        {
            //Get the request data
            $data = $request->all();
            //Filter the actions
            //UPDATE
            if( $data['action'] == 'update' )
            {
                if( $this->__updateUser( $request->all() ) )
                {
                    return response()->json(['response' => 'true' ]);   
                }
            }
            //BLOCK
            if( $data['action'] == 'block' )
            {

                if( $this->__blockUser( $request->all() ) )
                {
                    return response()->json(['response' => 'true' ]);   
                }
            }
            //DELETE
            if( $data['action'] == 'delete' )
            {

                if( $this->__deleteUser( $request->all() ) )
                {
                    return response()->json(['response' => 'true' ]);   
                }
            }
        }
        return response()->json(['response' => 'false' ]);   
    }

    /**
     * Update user info
     *
     * @return \Illuminate\Http\Response
     */
    private function __updateUser($param)
    {
        if( !empty($param) )
        {
            $data = $param['data'];
            $users = User::find($data["id"]);
            if( !empty($users) )
            {
                $users->name = $data['name'];
                $users->email = $data['email'];
                $users->role = $data['role'];
                $users->updated_at = Date("Y-m-d H:i:s");
                //Save the data
                if( $users->save() )
                {
                    return true;
                }
                return false;
            }
            return false;
        }
    }

    /**
     * Block user
     *
     * @return \Illuminate\Http\Response
     */
    private function __blockUser($param)
    {
        if( !empty($param) )
        {
            $data = $param['data'];
            $users = User::find($data["id"]);
            if( !empty($users) )
            {
                if( $data["type"] == "block" )
                {
                    $users->is_active = 0;    
                }
                elseif( $data["type"] == "unblock" )
                {
                    $users->is_active = 1;
                }
                
                $users->updated_at = Date("Y-m-d H:i:s");
                //Save the data
                if( $users->save() )
                {
                    return true;
                }
                return false;
            }
            return false;
        }
    }

    /**
     * Delete user
     *
     * @return \Illuminate\Http\Response
     */
    private function __deleteUser($param)
    {
        if( !empty($param) )
        {
            $data = $param['data'];
            $users = User::find($data["id"]);
            if( !empty($users) )
            {
                $users->is_delete = 1;
                $users->updated_at = Date("Y-m-d H:i:s");
                //Save the data
                if( $users->save() )
                {
                    return true;
                }
                return false;
            }
            return false;
        }
    }
}
