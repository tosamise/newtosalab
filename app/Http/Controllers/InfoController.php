<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Http\Requests;
use Session;
use Redirect;
use App\Camera;
use App\User;
use App\Film;
use App\Roll;
use App\Lab;

class InfoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['front','ajax']]);
    }

    /**
     * Show the camera list
     *
     * @return \Illuminate\Http\Response
     */
    public function list_camera()
    {
        //Get all camera record
        $Camera = new Camera;
        $cameras = $Camera->getAllCamera();
        //Get data for selectbox
        $User = new User;        
        $users = $User->GetListUser();

        return view('admin.info.list-camera')->with( compact('cameras','users') );
    }

    /**
     * Show the film list
     *
     * @return \Illuminate\Http\Response
     */
    public function list_film()
    {
        $Film = new Film;
        $films = $Film->getAllFilm();

        return view('admin.info.list-film')->with( compact('films') );
    }

    /**
     * Show the roll list
     *
     * @return \Illuminate\Http\Response
     */
    public function list_roll()
    {
        //Get all roll records
        $Roll = new Roll;
        $rolls = $Roll->getAllRoll();

        //Get data for selectbox
        $Film = new Film;
        $films = $Film->getListFilm();
        $Camera = new Camera;
        $cameras = $Camera->getListCamera();
        $Lab = new Lab;
        $labs = $Lab->getListLab();

        //Render data to view
        return view('admin/info/list-roll')->with( compact('rolls','films','cameras','labs') );
    }


    /**
     * Add and edit camera
     *
     * @return \Illuminate\Http\Response
     */
    public function store_camera(Request $request)
    {
        //Create rule to validation
        $rules = array(
            'cam_name' => 'required',
            'bought_price' => 'required',
            'cam_image' => 'mimes:jpeg,jpg,png,gif | max:5000',
        );
        $messages = array(
            'required' => 'The :attribute field is required.',
        );
        $validator = Validator::make(Input::all(), $rules, $messages);

        //Check the validation
        if ($validator->fails()) 
        {
            $request->session()->flash('alert-warning', 'Error input data');
            return Redirect::to('/admin/info/camera/')
                ->withErrors($validator);
        } 
        else 
        {
            $data_file = $request->file('cam_image');
            $uploaded_filename = "";
            //Check image is uploaded
            if( !empty( $data_file ) )
            {
                //Create folder if not existed
                if (!is_dir( public_path().'/images/camera/' ))
                {
                  mkdir( public_path().'/images/camera/', 0777 );
                }
                //Create image param
                $extension = Input::file('cam_image')->getClientOriginalExtension();
                $size = Input::file('cam_image')->getSize();
                
                //Set upload file info
                $destinationPath = public_path().'/images/camera/';
                $uploaded_filename = Input::file('cam_image')->getClientOriginalName();
                //Upload file
                $result = Input::file('cam_image')->move($destinationPath, $uploaded_filename);
            }

            //Get the request data
            $request_data = $request->all();

            //Add function
            if( $request_data['flag'] === 'add' )
            {
                //Create new entity
                $cameras = new Camera;
                //Prepage data
                $cameras->cam_name = $request_data["cam_name"];
                $cameras->cam_type = $request_data["cam_type"];
                $cameras->cam_image = (!empty($uploaded_filename)?$uploaded_filename:"no-camera.jpg");
                $cameras->lens = $request_data["lens"];
                $cameras->cam_series = $request_data["cam_series"];
                $cameras->bought_price = str_replace(',','', $request_data["bought_price"] );;
                $cameras->belong_to = $request_data["belong_to"];
                $cameras->is_sold = '0';
                //Execute the sql
                $result = $cameras->save();
                //Redirect options
                if( $result )
                {
                    $request->session()->flash('alert-success', 'Camera info was successful added!');
                    return Redirect::to('/admin/info/camera/');
                }
            }

            //Edit function
            if( $request_data['flag'] === 'edit' )
            {
                $cameras = Camera::find($request_data["id"]);
                if( !empty($cameras) )
                {
                    if(!empty($uploaded_filename) )
                    {
                        $cameras->cam_image = $uploaded_filename;
                    }
                    $cameras->cam_name = $request_data['cam_name'];
                    $cameras->cam_type = $request_data['cam_type'];
                    $cameras->lens = $request_data['lens'];
                    $cameras->bought_price = str_replace(',','', $request_data['bought_price'] );
                    $cameras->belong_to = $request_data['belong_to'];
                    $cameras->cam_series = $request_data['cam_series'];
                    $cameras->updated_at = Date("Y-m-d H:i:s");
                    //Save the data
                    if( $cameras->save() )
                    {
                        $request->session()->flash('alert-success', 'Camera info was successful updated!');
                        return Redirect::to('/admin/info/camera/');
                    }
                }
            }
        }
    }

    /**
     * Add and edit film
     *
     * @return \Illuminate\Http\Response
     */
    public function store_film(Request $request)
    {
        //Create rule to validation
        $rules = array(
            'model' => 'required',
            'film_name' => 'required',
            'film_type' => 'required',
        );
        $messages = array(
            'required' => 'The :attribute field is required.',
        );
        $validator = Validator::make(Input::all(), $rules, $messages);
        //Check the validation
        if ($validator->fails())
        {
            $request->session()->flash('alert-warning', 'Error input data');
            return Redirect::to('/admin/info/film/')
                ->withErrors($validator);
        }
        else
        {
            $data_file = $request->file('film_image');
            $uploaded_filename = "";
            //Check image is uploaded
            if( !empty( $data_file ) )
            {
                //Create folder if not existed
                if (!is_dir( public_path().'/images/film/' ))
                {
                    mkdir( public_path().'/images/film/', 0777 );
                }
                //Create image param
                $extension = Input::file('film_image')->getClientOriginalExtension();
                $size = Input::file('film_image')->getSize();

                //Set upload file info
                $destinationPath = public_path().'/images/film/';
                $uploaded_filename = Input::file('film_image')->getClientOriginalName();
                //Upload file
                $result = Input::file('film_image')->move($destinationPath, $uploaded_filename);
            }

            //Get the request data
            $request_data = $request->all();

            //Add function
            if( $request_data['flag'] === 'add' )
            {
                //Create new entity
                $films = new Film;
                //Prepage data
                $films->model = $request_data["model"];
                $films->film_type = $request_data["film_type"];
                $films->film_image = (!empty($uploaded_filename)?$uploaded_filename:"no-film.jpg");
                $films->film_name = $request_data["film_name"];
                $films->exp = $request_data["exp"];
                $films->iso = $request_data["iso"];
                $films->made_in = $request_data["made_in"];
                $films->is_discontinue= '0';
                //Execute the sql
                $result = $films->save();
                //Redirect options
                if( $result )
                {
                    $request->session()->flash('alert-success', 'Film info was successful added!');
                    return Redirect::to('/admin/info/film/');
                }
            }

            //Edit function
            if( $request_data['flag'] === 'edit' )
            {
                $films = Film::find($request_data["id"]);
                if( !empty($films) )
                {
                    if(!empty($uploaded_filename) )
                    {
                        $films->film_image = $uploaded_filename;
                    }
                    $films->model = $request_data["model"];
                    $films->film_type = $request_data["film_type"];
                    $films->film_name = $request_data["film_name"];
                    $films->exp = $request_data["exp"];
                    $films->iso = $request_data["iso"];
                    $films->made_in = $request_data["made_in"];
                    $films->updated_at = Date("Y-m-d H:i:s");
                    //Save the data
                    if( $films->save() )
                    {
                        $request->session()->flash('alert-success', 'Film info was successful updated!');
                        return Redirect::to('/admin/info/film/');
                    }
                }
            }
        }
    }

    /**
     * Add and edit roll
     *
     * @return \Illuminate\Http\Response
     */
    public function store_roll(Request $request)
    {
        //Create rule to validation
        $rules = array(
            'code_film' => 'required | unique:rolls',
            'lab_id' => 'required',
            'camera_id' => 'required',
            'film_id' => 'required',
        );

        $messages = [
            'code_film.required' => 'The film code is required',
            'code_film.unique' => 'The film code was already existed',
            'lab_id.required' => 'Please select lab',
            'camera_id.required' => 'Please select camera',
            'film_id.required' => 'Please select film type',
        ];

        $validator = Validator::make(Input::all(), $rules, $messages);
        
        //Get the request data
        $request_data = $request->all();

        //Check the validation
        if ($validator->fails())
        {
            //Set global error message
            $request->session()->flash('alert-warning', 'Error input data');
            
            return Redirect::to('/admin/info/roll/')->withInput()->withErrors($validator);
        }
        else
        {
            //Add function
            if( $request_data['flag'] === 'add' )
            {
                //Create new entity
                $rolls = new Roll;
                //Prepage data
                $rolls->code_film = $request_data["code_film"];
                $rolls->lab_id = $request_data["lab_id"];
                $rolls->camera_id = $request_data["camera_id"];
                $rolls->film_id = $request_data["film_id"];
                $rolls->is_delete = '0';
                //Execute the sql
                $result = $rolls->save();
                //Redirect options
                if( $result )
                {
                    $request->session()->flash('alert-success', 'Roll info was successful added!');
                    return Redirect::to('/admin/info/roll/');
                }
            }

            //Edit function
            if( $request_data['flag'] === 'edit' )
            {
                $rolls = Film::find($request_data["id"]);
                if( !empty($rolls) )
                {
                    if(!empty($uploaded_filename) )
                    {
                        $rolls->film_image = $uploaded_filename;
                    }
                    $rolls->model = $request_data["model"];
                    $rolls->film_type = $request_data["film_type"];
                    $rolls->film_name = $request_data["film_name"];
                    $rolls->exp = $request_data["exp"];
                    $rolls->iso = $request_data["iso"];
                    $rolls->made_in = $request_data["made_in"];
                    $rolls->updated_at = Date("Y-m-d H:i:s");
                    //Save the data
                    if( $rolls->save() )
                    {
                        $request->session()->flash('alert-success', 'Film info was successful updated!');
                        return Redirect::to('/admin/info/film/');
                    }
                }
            }
        }
    }


    /**
     * Do the ajax
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax(Request $request)
    {
        //Initalize the model object
        $Roll = new Roll;
        $Image = new Image;
        $Film = new Film;
        $Camera = new Camera;

        //Do the action
        if ($request->isMethod('post'))
        {
            //Get the request data
            $data = $request->all();
            //Camera information
            if( $data['kind'] == 'camera' )
            {
            	if( $data['action'] == 'sold' )
	            {
	                if( $Camera->soldCamera( $request->all() ) )
	                {
	                    return response()->json(['response' => 'true' ]);   
	                }
	            }
                elseif( $data['action'] == 'getImage' )
                {
                    return response()->json( $Image->getImage( $request->all() ) );
                }
            }
            //Film information
            elseif( $data['kind'] == 'film' )
            {
                if( $data['action'] == 'discont' )
                {
                    if( $Film->discontFilm( $request->all() ) )
                    {
                        return response()->json(['response' => 'true' ]);   
                    }
                }
                elseif( $data['action'] == 'getImage' )
                {
                    return response()->json( $Image->getImage( $request->all() ) );
                }
            }
            //Roll information
            elseif ( $data['kind'] == 'roll' )
            {
                if( $data['action'] == 'checkCodeFilm' )
                {   
                    return response()->json( $Roll->checkCodeFilm( $request->all() ) );   
                }	
            }
            //Lab information
            elseif ( $data['kind'] == 'lab' )
            {
                if( $data['action'] == 'close' )
                {
                    if( $this->__closeLab( $request->all() ) )
                    {
                        return response()->json(['response' => 'true' ]);   
                    }
                }
                elseif( $data['action'] == 'getImage' )
                {
                    return response()->json( $Image->getImage( $request->all() ) );
                }
            }
        }
        return response()->json(['response' => 'false' ]);   
    }

}

?>
