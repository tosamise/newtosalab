<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Frontend */
Route::get('/', 'GalleryController@front');
//Debug route
Route::get('/dd','HomeController@ddFunction');

/* Backend page */
Route::group(['prefix' => 'admin'], function () 
{
	//Auth page: login, register, password....
	Auth::routes();
    //Dashboard - Overview
	Route::get('home', 'HomeController@index');
	Route::get('/', 'HomeController@index');
	//Gallery
	Route::get('gallery', 'GalleryController@index')->name('gallery');
	Route::post('ajax-gallery', 'GalleryController@ajax');
	Route::get('/gallery/edit/{image_id}', [
	    'as'   => 'gallery.edit',
	    'uses' => 'GalleryController@edit'
	]);
	Route::post('/gallery/update/{image_id}', 'GalleryController@update');
	//InfoController
	Route::post('ajax-info', 'InfoController@ajax');
	//CAMERAS
	Route::get('/info/camera/', [
	    'as'   => 'info.list-camera',
	    'uses' => 'InfoController@list_camera'
	]);
	Route::post('info/camera/store', 'InfoController@store_camera');
    //FILMS
    Route::get('/info/film/', [
        'as'   => 'info.list-film',
        'uses' => 'InfoController@list_film'
    ]);
    Route::post('info/film/store', 'InfoController@store_film');
    //ROLLS
    Route::get('/info/roll/', [
        'as'   => 'info/list-roll',
        'uses' => 'InfoController@list_roll'
    ]);
    Route::post('info/roll/store', 'InfoController@store_roll');


	//User
	Route::get('list-user', 'UserController@list');
	Route::post('ajax-user', 'UserController@ajax');
	Route::post('upload', 'UserController@upload');
});
