<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file_name')->nullable();
            $table->string('file_small')->nullable();
            $table->string('title')->nullable();
            $table->string('apature',10)->nullable();
            $table->string('shutter',10)->nullable();
            $table->unsignedInteger('roll_id')->nullable();
            $table->unsignedInteger('taken_by')->nullable();
            $table->string('coordinates',50)->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('is_delete');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
