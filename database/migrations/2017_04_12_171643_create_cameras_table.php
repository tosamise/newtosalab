<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCamerasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cameras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cam_name',50)->nullable();
            $table->string('cam_type',50)->nullable();
            $table->string('lens',50)->nullable();
            $table->string('cam_image')->nullable();
            $table->decimal('bought_price')->nullable();
            $table->unsignedInteger('belong_to')->nullable();
            $table->string('cam_series')->nullable();
            $table->timestamps();
            $table->tinyInteger('is_sold');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cameras');
    }
}
