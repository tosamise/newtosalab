<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rolls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code_film',10)->nullable();
            $table->unsignedInteger('lab_id')->nullable();
            $table->unsignedInteger('camera_id')->nullable();
            $table->unsignedInteger('film_id')->nullable();
            $table->timestamps();
            $table->tinyInteger('is_delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rolls');
    }
}
