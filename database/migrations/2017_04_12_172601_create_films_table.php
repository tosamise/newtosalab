<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            $table->increments('id');
            $table->string('model',50)->nullable();
            $table->string('film_type',50)->nullable();
            $table->string('film_name',50)->nullable();
            $table->string('exp',50)->nullable();
            $table->string('iso',50)->nullable();
            $table->string('film_image')->nullable();
            $table->string('made_in',50)->nullable();
            $table->timestamps();
            $table->tinyInteger('is_unproductive');
            $table->tinyInteger('is_delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}
