<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lab_name',50)->nullable();
            $table->string('lab_address')->nullable();
            $table->string('lab_website')->nullable();
            $table->string('lab_image')->nullable();
            $table->string('coordinates',50)->nullable();
            $table->timestamps();
            $table->tinyInteger('is_close');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('labs');
    }
}
